# GCE main content extraction Framework
 This is a implimentation of GCE on the [main content extraction assessemnt framework](https://github.com/dreamwayjgs/main-content-extraction-assessment-framework).

 GCE arxiv: https://arxiv.org/abs/2110.14164

# Instructions
## Instructions for running or developement
  Please see the README.md of the [main content extraction assessemnt framework](https://github.com/dreamwayjgs/main-content-extraction-assessment-framework).

## GCE module
GCE module is in the chrome_extension folder. We provide the extraction function in <code>src/inject/Extraction/extraction.ts</code> and parsers in <code>src/inject/Evaluation/parsers/hyucentroid.ts</code>.
### Extraction (extraction.ts)
The function <code>extract</code> extracts the main content in the current web page.

  ```typescript
  import {extract} from './extraction';

  const mainContent = extract();  
  //  mainContent: {
  //    name: string,
  //    desc?: string,
  //    el: HTMLElement, 
  //    centerName?: string, 
  //    best?: boolean, 
  //    seed: HTMLElement
  //  }
  
  ```

### Evaluation (parsers/hyucentroid.ts)
The creating parse instance and its <code>measure</code> method returns evaluation results. The <code>values</code> property of the evaluation result depends on the type of the measurement. For example, LCS F1 score has precision, recall, and F1-score in the <code>values</code> object.

  ```typescript
  import { getParser } from "./parsers";
  
  const parser = getParser(extractionResult); 
  const evalResult = parser.measure();

  //  evalResult: {
  //    name: string,
  //    values: any,
  //  }  
  ```