import { getDefaultConfig, TaskConfig } from "./common";

export const TASK_NAME_CRAWL = 'crawl'

export interface CrawlConfig extends TaskConfig {
  timeoutAfterLoad: number
  timeoutAfterOpen: number
}

export function getCrawlConfig(config?: Partial<CrawlConfig>): CrawlConfig {
  return {
    timeoutAfterLoad: config?.timeoutAfterLoad || 5000,
    timeoutAfterOpen: config?.timeoutAfterOpen || 5000,
    ...getDefaultConfig(config)
  }
}