import { getDefaultConfig, TaskConfig } from "./common";

export const TASK_NAME_CURATION = 'curation'

export interface CurationConfig extends TaskConfig {

}

export function getCurationConfig(config?: Partial<CurationConfig>): CurationConfig {
  return getDefaultConfig(config)
}