import { TaskConfig } from "./common"

export const TASK_NAME_EVALUATION = 'evaluation'

export interface EvaluationConfig extends TaskConfig {
  experiment: boolean
  name: string
  openOrigin: boolean
  query?: { [key: string]: string }
}

export function getEvaluationConfig(config?: Partial<EvaluationConfig>): EvaluationConfig {
  const defaultConfig: EvaluationConfig = {
    start: config?.start || 0,
    end: config?.end || -1,
    watch: config?.watch || false,
    auto: config?.auto || false,
    action: "Evaluation",
    name: config?.name || 'hyucentroid',
    experiment: config?.experiment || false,
    openOrigin: config?.openOrigin || false,
    query: config?.query || {}
  }
  return defaultConfig
}
