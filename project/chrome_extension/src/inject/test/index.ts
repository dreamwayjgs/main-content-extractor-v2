import { HTMLPolygon } from "../common/polygon"
import { createMarker } from "../Extraction/center"
import { elementVertexes } from "../Extraction/domAnalysis"
import { isInside, getAreaSize, getOverlapPoints, getOverlapSize } from "overlap-area";

export function initTests() {
  console.info("INIT START")
  polygonTest()
}

function polygonTest() {

  chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    const { work, action } = msg
    if (work === 'test' && action === 'polygon') {
      console.log("스타트 폴리곤 테스트")
      const boxA = document.getElementById('a')!
      const boxB = document.getElementById('b')!
      const boxC = document.getElementById('c')!
      const boxD = document.getElementById('d')!

      const polygonA = new HTMLPolygon(boxA)
      const polygonB = new HTMLPolygon(boxB)
      const polygonC = new HTMLPolygon(boxC)
      const polygonD = new HTMLPolygon(boxD);

      console.log("각각 넓이", polygonA.area)
      console.log("각각 넓이", polygonB.area)
      console.log("각각 넓이", polygonC.area)
      console.log("각각 넓이", polygonD.area)

      console.log("A와 D의 관계를 알아봅시다")
      const pa = polygonA.p3
      const pd = polygonD.p3

      if (getOverlapSize(pa, pd) === 0) {
        console.log("관계가 없음")
      } else if (polygonA.includes(polygonD)) {
        console.log("a 가 d 보다 더 큼")
      } else if (polygonD.includes(polygonA)) {
        console.log("d 가 a 보다 큼")
      } else if (getOverlapSize(pa, pd) === getAreaSize(pa)) {
        console.log("동치")
      } else {
        console.log("이만큼 겹침", getOverlapSize(pa, pd))
      }


      console.log(getOverlapSize(pa, pd))

        ;


      console.log("A랑 겹치는 케이스", polygonA.intersectionArea([polygonB, polygonC, polygonD]))
      console.log("전체합? 케이스", polygonA.unionArea([polygonB, polygonC, polygonD]))
      console.log("A와 IOU", polygonA.intersectionArea([polygonB, polygonC, polygonD]) / HTMLPolygon.unionArea([polygonA, polygonB, polygonC, polygonD]))
      console.log('내부', HTMLPolygon.intersectionArea([polygonA, polygonB, polygonC, polygonD]))
      console.log('총합', HTMLPolygon.unionArea([polygonA, polygonB, polygonC, polygonD]))


    }
  })
}
