import { Answer } from "../../types/models"
import { markAnswers } from "../Evaluation/evaluation"
import { extract } from "./extraction"
import { isProbablyReaderable, Readability } from '@mozilla/readability'
import { boilernetToHyuIndex } from "./boilernet"
import { extractBasicFeatures, extractElemCenterToDocumentCenters, extractAnswerTagged, extractDocFeatures } from "./features"
import { getElementByHyuIndex } from "../common"
import { Border } from "../Overlay/border"
import { ExtractionConfig } from "../../common/configs/extraction"


export function initExtraction() {
  console.log("[INIT] Extraction Ready")
  chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    const { work, action, answers, originalUrl, config, pid } = message as {
      work: string, action: string, answers: Answer[], originalUrl: string, config: ExtractionConfig, pid: string
    }
    if (work === 'extraction' && action === 'markAnswer') {
      console.log("Answer", answers)
      markAnswers(answers)
      sendResponse({ status: 'ok', action, work, pid })
    } else if (work === 'extraction' && action === 'mozilla') {
      const isReadable = isProbablyReaderable(document)
      const documentClone = document.cloneNode(true);
      const article = new Readability(documentClone as Document).parse()
      const answerIndex = article && article.content ? getAnswerIndex(article.content) : null

      const result = {
        isReadable: isReadable,
        article: article,
        answerIndex: answerIndex
      }
      const response = {
        status: 'ok',
        data: JSON.stringify(result),
        pid
      }
      console.log("Mozilla", response)
      if (answerIndex) {
        const resultEl = getElementByHyuIndex(answerIndex)
        console.log(resultEl)
        const border = new Border()
        border.text = 'Mozilla'
        border.cover(resultEl)
        markAnswers(answers)
      }
      sendResponse(response)
    } else if (work === 'extraction' && action === 'centroid') {
      console.log("센트로이드 추출", config)
      try {
        const result = extract(config, originalUrl)
        const response = {
          status: 'ok',
          pid: pid,
          data: JSON.stringify({
            ...result,
            el: result.el.outerHTML,
            seed: result.seed.outerHTML
          })
        }
        console.log("최종", result.seed, result.el)
        sendResponse(response)
      }
      catch (err) {
        console.error(err)
        sendResponse({
          status: 'error',
          error: err,
          pid
        })
      }
    } else if (work === 'extraction' && action === 'boilernet') {
      const { prediction } = message as { prediction: number[] }
      const result = boilernetToHyuIndex(prediction)
      sendResponse({
        status: 'ok',
        data: result,
        pid
      })
    } else if (work === 'extraction' && action === 'features') {
      const basicFeatures = extractBasicFeatures()
      const answerTagged = extractAnswerTagged(message.answers)
      const distanceToCenters = extractElemCenterToDocumentCenters()
      sendResponse({
        status: 'ok',
        work: work,
        action: action,
        data: {
          basicFeatures: basicFeatures,
          answerTagged: answerTagged,
          distanceToCenters: distanceToCenters
        },
        pid
      })
    } else if (work === 'extraction' && action === 'brave') {
      if (document.body.firstElementChild && document.body.firstElementChild.id === 'article') {
        console.log("브레이브가 뽑아냇따!")
        sendResponse(JSON.stringify({
          content: document.body.innerHTML,
          isReadable: true
        }))
      }
    } else if (work === 'extraction' && action === 'docFeatures') {
      const docFeatures = extractDocFeatures()
      sendResponse({
        status: 'ok',
        work: work,
        action: action,
        data: {
          documentFeatures: docFeatures
        },
        pid
      })
    }
    return true
  })
}

function getAnswerIndex(contentString: string) {
  const content = createElementFromHTML(contentString).firstElementChild! as HTMLElement
  const answerIndex = content.getAttribute('hyu')
  return answerIndex
}

function createElementFromHTML(htmlString: string): HTMLElement {
  const div = document.createElement('div');
  div.innerHTML = htmlString.trim();

  // Change this to div.childNodes to support multiple top-level nodes
  return div.firstElementChild as HTMLElement
}