import { filter, flatten, mean } from "lodash"
import { createMarker, examineGrids, getDocumentCenter, getScreenCenter, iterGrids, toggleGrids } from "./center"
import { elementCenter, elementClosestDistance, elementContains, elementDistance, elementOverlaps, expandToMaincontent } from "./domAnalysis"
import { countAnchorArea, countAnchorAreaRatio } from "./linkAnalysis/area"
import $ from 'jquery'
import { Border } from "../Overlay/border"
import { ElementPosition } from "../../types"
import { reportCandidates } from "./domAnalysis/extractEvaluation"
import { nodesUnder } from "../../common"
import { ExtractionConfig } from "../../common/configs/extraction"


const LINK_AREA_RATIO_HEAVY = 0.75
const LINK_AREA_RATIO_LIGHT = 0.5

export function extractCenter(config: ExtractionConfig, originalUrl?: string) {
  const innerHref = originalUrl || window.location.href
  console.log("내부 링크 취급", innerHref)

  const { gridRow: row, gridColumn: col, gridCoef: coef, linkRatio } = config

  markTitles() // 아무 역할도 안 하는데..?
  markFixed()
  const rawTextNodes = nodesUnder(document.body, { showText: true })
  wrappingTextNodes(rawTextNodes)
  examineGrids(row, col, coef)
  countLinks(innerHref, linkRatio, row, col, coef)
  const textNodes = Array.from(document.querySelectorAll('span.hyu.wrapped:not([hyunav]):not([hyu-fixed])'))

  console.log('target spans', textNodes.length)
  const centroids = markGridCentroid(row, col, coef)
  return {
    centroids: centroids,
    textNodes: textNodes
  }
}

export function extract(config: ExtractionConfig, originalUrl?: string) {
  console.log("이렇게 돌거야", config.name, config)
  const { centroids, textNodes } = extractCenter(config, originalUrl)

  const seeds = centroids.map(c => {
    const minDistElem = examineTextNodes(textNodes, c)
    // 시각화를 원하면
    // const border = new Border({ color: '#ff1493', thickness: '2px' })
    // border.cover(minDistElem.el as HTMLElement)
    // border.text = 'CLOESET ELEMENT - ' + c.name
    return {
      name: c.name,
      ...minDistElem
    }
  })
  console.log("보이는지?", ...seeds.map(({ dist, el }) => {
    return [dist, el]
  }))
  const extracted = flatten(seeds.map(({ name, el }) => expandToMaincontent(el as HTMLElement, config, name)))
  const result = reportCandidates(extracted, centroids)

  toggleGrids()
  return result
}

function markTitles(hSelector = 'h1,h2,h3,h4,h5') {
  const hs = document.querySelectorAll(hSelector)
  hs.forEach(h => {
    h.setAttribute('hyutitle', 'true')
    h.querySelectorAll('*').forEach(x => {
      x.setAttribute('hyutitle', 'true')
    })
  })
}

function markFixed() {
  document.body.querySelectorAll(":not(.hyu)").forEach(el => {
    if (window.getComputedStyle(el).position === 'fixed' ||
      window.getComputedStyle(el).position === 'absolute') {
      el.setAttribute('hyu-fixed', 'true')
      el.querySelectorAll('*').forEach(d => d.setAttribute('hyu-fixed', 'true'))
    }
  })
}

function wrappingTextNodes(textNodes: Node[]) {
  textNodes.forEach(textNode => {
    const txt = textNode as CharacterData
    if (txt.data.trim() === '') return

    const newSpan = document.createElement('span')
    newSpan.appendChild(txt.cloneNode())
    newSpan.className = 'hyu wrapped'
    txt.after(newSpan)
    txt.remove()
  })
}


function examineTextNodes(textNodes: Node[], centroid: { name: string, position: ElementPosition }) {
  const minDistEl = {
    dist: Infinity,
    el: textNodes[0]
  }
  console.log("처리대상", textNodes.length)
  textNodes.some(el => {
    try {
      const dist = elementClosestDistance(centroid.position, el as HTMLElement)
      if (dist < minDistEl.dist) {
        minDistEl.dist = dist
        minDistEl.el = el
      }
    }
    catch (err) {
      console.error(el)
    }
  })
  return minDistEl
}

function examineTextNodesCenter(textNodes: Node[], centroid: { name: string, position: ElementPosition }) {
  const minDistEl = {
    dist: Infinity,
    el: textNodes[0]
  }
  textNodes.some(el => {
    try {
      const dist = elementDistance(el as HTMLElement, centroid.position)
      if (dist < minDistEl.dist) {
        minDistEl.dist = dist
        minDistEl.el = el
      }
    }
    catch (err) {
      console.error(el)
    }
  })
  return minDistEl
}

function countLinks(innerHref: string, linkRatio = 0.75, row: number, col: number, coef: number) {
  console.log("카운팅 링크", linkRatio, row, col, coef)
  const { visibleAnchorList, expandedAnchorList } = countAnchorArea()
  const anchorAreaRatioList = countAnchorAreaRatio()

  anchorAreaRatioList.forEach(({ ratio, el }, index, arr) => {
    el.setAttribute('hyu-link-area-ratio-rank', `${index}/${arr.length}`)
  })


  // 시각화 플래그 필요
  // filter(anchorAreaRatioList, o => o.ratio > LINK_AREA_RATIO_LIGHT).forEach(({ ratio, el }) => {
  //   $(el).css('outline', '2px dashed #9ACD32')
  // })
  // // 상위 50% 비율
  // filter(anchorAreaRatioList, o => o.ratio > LINK_AREA_RATIO_HEAVY).forEach(({ ratio, el }) => {
  //   $(el).css('outline', '4px dashed orange')
  // })

  document.querySelectorAll('span.hyu.wrapped').forEach(node => {
    const parent = node.closest('[hyu-fixed]')
    if (parent) {
      node.setAttribute('hyu-fixed', 'true')
    }
  })

  document.querySelectorAll('span.hyu.wrapped').forEach(node => {
    const parent = node.closest('[hyu-link-area-ratio]')
    if (parent) {
      const ratio = parent.getAttribute('hyu-link-area-ratio')
      if (ratio && parseFloat(ratio) > linkRatio) node.setAttribute('hyunav', 'heavy')
      if (ratio && parseFloat(ratio) > LINK_AREA_RATIO_LIGHT) node.setAttribute('hyunav', 'light')
    }
  })

  const navCandidatesHeavy = filter(anchorAreaRatioList, o => o.ratio > linkRatio)
  // const navCandidatesLight = filter(anchorAreaRatioList, o => o.ratio > LINK_AREA_RATIO_LIGHT && o.ratio <= linkRatio)
  const grids = iterGrids(row, col, coef)
  for (const { x, y, el: grid } of grids) {
    // navCandidatesLight.some(({ ratio, el }) => {
    //   const overlap = elementOverlaps(grid, el) || elementContains(grid, el)
    //   if (overlap) {
    //     grid.style.backgroundColor = `rgba(160, 160, 0, 0.8)`
    //     $(grid).attr('deadzone', 'true')
    //     return true
    //   }
    // })
    navCandidatesHeavy.some(({ ratio, el }) => {
      const overlap = elementOverlaps(grid, el) || elementContains(grid, el)
      if (overlap) {
        const borderId = parseInt($(grid).attr('border')!)
        const borders = filter(Border.borders, { 'id': borderId })
        borders?.forEach(border => {
          border.color = 'red'
        })
        grid.style.backgroundColor = `rgba(255, 0, 0, 0.5)`
        $(grid).attr('deadzone', 'true')

        return true
      }
    })
    // if (w >= 5 && h >= 6) break;
  }

  console.log("=====REPORT=====")
  console.log(visibleAnchorList)
  console.log(expandedAnchorList)
  console.log(anchorAreaRatioList)
  console.log("=====END=====")

}

function markGridCentroid(row: number, col: number, coef: number): { name: string, position: ElementPosition }[] {
  const grids = iterGrids(row, col, coef)
  const lefts: number[] = []
  const tops: number[] = []
  let curWidthGrid: number = 0
  for (const { el, widthGrid } of grids) {
    curWidthGrid = widthGrid
    if ($(el).attr('deadzone') === 'false') {
      const { left, top } = elementCenter(el)
      lefts.push(left)
      tops.push(top)
    }
  }
  const centroids: { name: string, position: ElementPosition }[] = []

  createMarker([mean(lefts), mean(tops)], '#FFA500', 'C1')
  centroids.push({ name: 'centroid', position: { left: mean(lefts), top: mean(tops) } })

  const [screenLeft, screenTop] = getScreenCenter()
  lefts.push(screenLeft)
  tops.push(screenTop)
  createMarker([mean(lefts), mean(tops)], '#FFA500', 'C2')
  centroids.push({ name: 'centerCentroid', position: { left: mean(lefts), top: mean(tops) } })

  const docCoef = Math.max(curWidthGrid - 2, 1) // doc 중심에 green  grid 있다고 가정
  const [docLeft, docTop] = getDocumentCenter()
  lefts.push(docLeft)
  tops.push(docTop * docCoef)
  createMarker([mean(lefts), mean(tops)], '#FFA500', 'C3')
  centroids.push({ name: 'centerDocCentroid', position: { left: mean(lefts), top: mean(tops) } })

  return centroids
}

function areaRatioVariation(anchorAreaRatioList: {
  ratio: number, el: HTMLElement
}[]) {
  console.log("전체", anchorAreaRatioList.length)
  console.log(`비율 0 `)
  const countZero = filter(anchorAreaRatioList, o => o.ratio === 0).length
  console.log(countZero, countZero / anchorAreaRatioList.length, '%')
  for (const standard of [...Array(10).keys()]) {
    console.log(`비율 ${standard / 10} - ${(standard + 1) / 10}`)
    const count = filter(anchorAreaRatioList, o => o.ratio > standard / 10 && o.ratio <= (standard + 1) / 10).length
    console.log(count, count / anchorAreaRatioList.length, '%')
  }
  console.log(`비율 1 - `)
  const countOverOne = filter(anchorAreaRatioList, o => o.ratio > 1).length
  console.log(countOverOne, countOverOne / anchorAreaRatioList.length, '%')
}