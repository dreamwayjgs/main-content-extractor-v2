import { filter, find } from "lodash";
import { Answer, ExtractionResult } from "../../../types/models";
import { falsyFilter, nodesUnder } from "../../../common";
import { getElementByHyuIndex } from "../../common";
import { createMarker, getDocumentCenter, getDocumentSize, getElementSize, getScreenCenter, toggleGrids } from "../../Extraction/center";
import { elementDistance, elementInside } from "../../Extraction/domAnalysis";
import { extractCenter } from "../../Extraction/extraction";
import { expandedLinkArea } from "../../Extraction/linkAnalysis/area";
import { Border } from "../../Overlay/border";
import { getExtractionConfig } from "../../../common/configs/extraction";

function checkCentroid(answer: Answer) {
  const answerIndex = answer.hyuIndex
  const mainContentElem = getElementByHyuIndex(answerIndex)
  const mainContent = {
    domRect: mainContentElem.getBoundingClientRect(),
    hyuIndex: answerIndex
  }

  const border = new Border()
  border.cover(mainContentElem)

  const config = getExtractionConfig()

  const { centroids: centers } = extractCenter(config)
  centers.push({
    name: 'C_w',
    position: { left: getScreenCenter()[0], top: getScreenCenter()[1] }
  })
  centers.push({
    name: 'C_d',
    position: { left: getDocumentCenter()[0], top: getDocumentCenter()[1] }
  })

  const distAndCenters = centers.map(center => {
    createMarker([center.position.left, center.position.top], '#FFA500', center.name)
    const centerInMainContent = elementInside(mainContentElem, { left: center.position.left, top: center.position.top })
    return {
      centerInMainContent: centerInMainContent,
      name: center.name,
      dist: elementDistance(mainContentElem, center.position),
      left: center.position.left,
      top: center.position.top
    }
  })


  return [mainContent, falsyFilter(distAndCenters), getDocumentSize()]
}

function computeNavLinkDensity(answer: Answer, visibleOnly = false) {
  const navElem = getElementByHyuIndex(answer.hyuIndex)

  console.log("NAV Target on INdex", answer.hyuIndex, navElem)

  // Typical 1: link node / node
  const nodes = nodesUnder(navElem, { visibleOnly: visibleOnly })
  const anchorNodes = filter(nodes, node => {
    if (node === navElem) return false
    if (['A', 'BUTTON'].includes(node.nodeName)) return true
    if (node.nodeName === 'input' && (node as HTMLElement).getAttribute('type') === 'button')
      return true

    const classList = (node as HTMLElement).classList
    const btn = find(classList, name => name.includes('btn'))
    const button = find(classList, name => name.includes('button'))
    if (btn && btn.length > 0) return true
    if (button && button.length > 0) return true

    return false
  })
  const linkRatioNode = nodes.length > 0 ? anchorNodes.length / nodes.length : 0

  console.log("Node가 없는건아니지", nodes)
  console.log("Anchor 노드는 말단만인가?", anchorNodes)
  console.log("첫번째들", nodes[0], anchorNodes[0])

  // console.log(nodes, anchorNodes, linkRatioNode)

  // Typical 2: link text / node text  
  const nodeText = navElem.textContent ? navElem.textContent.trim() : ''
  const anchorText = anchorNodes.reduce<string>((acc, cur) => {
    return cur.textContent ? acc + cur.textContent.trim() : acc
  }, '')

  const linkRatioText = nodeText.length > 0 ? anchorText.length / nodeText.length : 0

  // New!: link area (wrapper) / node
  const nodeArea = getElementSize(navElem)
  const expandedAnchorArea = anchorNodes.reduce<number>((acc, cur) => {
    const { size } = expandedLinkArea(cur as HTMLElement)
    return acc + size
  }, 0)

  const linkRatioArea = nodeArea > 0 ? expandedAnchorArea / nodeArea : 0

  console.assert(linkRatioNode >= 0, `linkRatioNode worng ${answer.pid} ${navElem}`)
  console.assert(linkRatioText >= 0, `linkRatioText worng ${answer.pid} ${navElem}`)
  console.assert(linkRatioArea >= 0, `linkRatioArea worng ${answer.pid} ${navElem}`)

  if (!(linkRatioNode >= 0 && linkRatioText >= 0 && linkRatioArea >= 0)) {
    console.error(linkRatioNode, linkRatioText, linkRatioArea)
    console.log(navElem)
    throw new Error("값 이상")
  }

  console.log("결과", linkRatioNode, linkRatioText, linkRatioArea)
  return {
    domRect: navElem.getBoundingClientRect(),
    hyuIndex: answer.hyuIndex,
    tagType: answer.tagType,
    visibleOnly: visibleOnly,
    linkRatioNode,
    linkRatioText,
    linkRatioArea,
  }
}

function navLinkResults(answers: Answer[]) {
  const results = answers.map(answer => {
    return {
      name: 'nav',
      answer: answer,
      allNodes: computeNavLinkDensity(answer, false),
      visibleNodes: computeNavLinkDensity(answer, true)
    }
  })
  return results
}

function viewExtraction(answers: Answer[], extracted: ExtractionResult[]) {
  const answerBorder = new Border({ color: 'red' })
  const mozEl = getElementByHyuIndex(answers[0].hyuIndex)
  answerBorder.cover(mozEl)
  answerBorder.text = 'Answer'

  const coverResults = (r: ExtractionResult, index: number) => {
    const border = new Border({ color: 'random' })
    border.text = r.name
    const el = getElementByHyuIndex(r.hyuIndex!)
    border.cover(el)
    console.log(index, "커버", el, r.name, r.best, border.color)
  }

  extracted.forEach(coverResults)

  extractCenter(getExtractionConfig({
    gridRow: 7,
    gridColumn: 8,
    gridCoef: 2
  }))
  toggleGrids()
}

export function runExperimentSet(answers: Answer[], extracted: ExtractionResult[]) {
  const mainContentElem = filter(answers, answer => answer.tagType === 'maincontent')
  const navElem = filter(answers, answer => answer.tagType === 'nav' || answer.tagType === 'maincontent')
  const results: any[] = []
  if (!!mainContentElem) {
    viewExtraction(mainContentElem, extracted)
  }

  // if (navElem.length > 0) {
  //   console.log("Target Nav Elems", navElem)
  //   results.push(...navLinkResults(navElem))
  // }
  return results
}