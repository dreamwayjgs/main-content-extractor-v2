import { TASK_NAME_EVALUATION } from "../../common/configs/evaluation"
import { EvaluationRequest } from "../../types/models"
import { evaluate, markAnswers } from "./evaluation"
import { runExperimentSet } from './experiments'

export function initEvaluation() {
  console.log("[INIT] Eval Ready")
  chrome.runtime.onMessage.addListener((message: EvaluationRequest, sender, sendResponse) => {
    const { work, action, answers, originalUrl, config, results: extracted, pid } = message
    console.log("[REQ] eval req", work, action)
    if (config && config?.experiment) {
      const results = runExperimentSet(answers, extracted)
      sendResponse({ status: 'ok', action: 'exp', data: results, pid: pid })
    } else {
      if (work === TASK_NAME_EVALUATION && action === 'markAnswer') {
        console.log(answers)
        console.log(originalUrl)
        markAnswers(answers)
        sendResponse({ status: 'ok', action: 'markAnswer', pid })
        return true
      }
      if (work === TASK_NAME_EVALUATION && action === 'evaluate') {
        try {
          const evalResult = evaluate(extracted, answers)
          sendResponse({ status: 'ok', data: evalResult, pid })
        }
        catch (err) {
          console.error("Error on Evaluation Request", err)
          sendResponse({ status: 'error', error: err, pid })
        }
        return true
      }
    }
  })
}

function getAnswerIndex(contentString: string) {
  const content = createElementFromHTML(contentString).firstElementChild! as HTMLElement
  const answerIndex = content.getAttribute('hyu')
  return answerIndex
}

function createElementFromHTML(htmlString: string): HTMLElement {
  const div = document.createElement('div');
  div.innerHTML = htmlString.trim();

  // Change this to div.childNodes to support multiple top-level nodes
  return div.firstElementChild as HTMLElement
}