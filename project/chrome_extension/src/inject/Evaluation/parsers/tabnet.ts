import { Parser } from "./parser";
import { falsyFilter } from "../../../common";
import { expandUntil } from "../../Extraction/domAnalysis";
import { BorderOptions } from "../../Overlay/border";
import { getElementByHyuIndex } from "../../common";

export class TabnetParser extends Parser {
  setElements() {
    const { content } = this.extractionResult as { content: number[] }
    if (content.length === 0) throw new Error("Parser Cannt Created: result lenght is 0")
    const elems = content.map(e => {
      try {
        const el = getElementByHyuIndex(e)
        // el.style.outline = '3px solid red'
        return el
      }
      catch (err) {
        console.error(err)
        return null
      }
    })
    return falsyFilter(elems)
  }
  setRoot() {
    if (this.elems.length === 0) throw new Error("Parser Cannot Created: root not defined")
    if (this.elems.length === 1) return this.elems[0]
    const parseName = this.name.split(/[<>]/)
    const category = parseName[0]
    const parseModel = parseName[1].split(',')
    const model = parseModel[0]
    const prob = parseModel[1]
    const group = parseName[2] === '' ? '' : '-group'

    const attrBase = `${category}-${model}-${prob}${group}`
    console.log(parseName, attrBase)
    const attrName = `${attrBase}-root`
    let countMax = 0
    let countMaxEl: HTMLElement | null = null
    this.elems.forEach(el => {
      expandUntil(el, (current) => {
        const rootCountAttr = current.getAttribute(attrName)
        const rootCount = rootCountAttr ? parseInt(rootCountAttr) + 1 : 1
        current.setAttribute(attrName, rootCount.toString())
        if (rootCount > countMax) {
          countMax = rootCount
          countMaxEl = current
        }
        return false
      })
    })
    if (countMaxEl) return countMaxEl;
    else {
      console.log("이런 경우가 있나?", this.elems)
      throw new Error("Parser Error");
    }
  }
  mark(borderOptions?: BorderOptions) {
    const options = borderOptions || { color: '#ff5733' }
    super.mark(options)
  }
}