import { uniq } from "lodash";
import { falsyFilter } from "../../../common";
import { getElementByHyuIndex } from "../../common";
import { BorderOptions } from "../../Overlay/border";
import { Metric, Parser } from "./parser";

export class HyuCentroidParser extends Parser {
  setElements() {
    const { hyuIndex } = this.extractionResult
    if (hyuIndex && hyuIndex !== -1) {
      return [getElementByHyuIndex(hyuIndex)]
    }
    console.error(`${this.name} Not Readable`)
    return []
  }
  setRoot() {
    const { hyuIndex } = this.extractionResult
    if (hyuIndex && hyuIndex !== -1) {
      return getElementByHyuIndex(hyuIndex)
    }
    console.error(`${this.name} Not Readable`)
    return document.body
  }
  mark(borderOptions?: BorderOptions) {
    const options = borderOptions || { color: '#53ff07' }
    super.mark(options)
  }
  measure(answers: { answerUser: string, el: HTMLElement }[]): Metric[] {
    const textNodes = this.textBlocks()
    const textElems = falsyFilter(textNodes.map(node => node.parentElement).map(el => {
      if (el && el.getAttribute) return el.getAttribute('hyu')
      return null
    }))
    console.log(textElems)
    console.log(textElems.length)
    console.log(uniq(textElems).length)
    return super.measure(answers)
  }
}