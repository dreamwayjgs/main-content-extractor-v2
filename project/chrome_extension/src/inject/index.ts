import { initCommonScripts } from "./common";
import { initCrawl } from "./Crawl";
import { initCuration } from "./Curation";
import { initEvaluation } from "./Evaluation";
import { initExtraction } from "./Extraction";
import { initTests } from "./test";


function main() {
  initCrawl()
  initCuration()
  initExtraction()
  initEvaluation()
  initCommonScripts()
  initTests()
}

main()