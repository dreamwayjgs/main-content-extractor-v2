import { ElementPosition } from '../../types'
import { elementVertexes } from '../Extraction/domAnalysis'
import { isInside, getAreaSize, getOverlapSize } from "overlap-area";

export class HTMLPolygon {
  p3: number[][]
  constructor(el: HTMLElement) {
    this.p3 = HTMLPolygon.elemToPolygonArray(el)
  }
  get area() {
    return getAreaSize(this.p3)
  }
  isOverLaps(polygon: HTMLPolygon | number[][]) {
    return getOverlapSize(this.p3, polygon instanceof HTMLPolygon ? polygon.p3 : polygon) ? false : true
  }
  includes(polygon: HTMLPolygon | number[][]) {
    const targets = polygon instanceof HTMLPolygon ? polygon.p3 : polygon
    return targets.map(pos => isInside(pos, this.p3)).every(e => e)
  }
  intersectionArea(targets: HTMLPolygon[]) {
    //분모
    const sum = targets.reduce((acc, cur) => acc + getOverlapSize(this.p3, cur.p3), 0)
    const innerIntersects = HTMLPolygon.intersectionArea(targets)
    return sum - innerIntersects
  }
  unionArea(targets: HTMLPolygon[]) {
    const sumOfEach = targets.reduce<number>((acc, cur) => acc + cur.area, 0)
    const sumOfintersect = targets.reduce((acc, cur) => acc + getOverlapSize(this.p3, cur.p3), 0)
    return sumOfEach - sumOfintersect
  }
  static elemToPolygonArray(el: HTMLElement) {
    const { lt, rt, rb, lb } = elementVertexes(el)
    return HTMLPolygon.leftTopToCoordArray([lt, rt, rb, lb])
  }
  static leftTopToCoordArray(positions: ElementPosition[]) {
    return positions.map(({ left, top }) => [left, top])
  }
  static overlapSize(A: HTMLPolygon | number[][], B: HTMLPolygon | number[][]) {
    const pA = A instanceof HTMLPolygon ? A.p3 : A
    const pB = B instanceof HTMLPolygon ? B.p3 : B
    return getOverlapSize(pA, pB)
  }
  static unionArea(els: HTMLPolygon[]) {
    //분자
    const sumOfEach = els.reduce<number>((acc, cur) => acc + cur.area, 0)
    console.log("생짜합", sumOfEach)
    return sumOfEach - HTMLPolygon.intersectionArea(els)
  }
  static intersectionArea(polygons: HTMLPolygon[]) {
    let overlapArea = 0;
    for (let i = 0; i < polygons.length; i++) {
      for (let j = i + 1; j < polygons.length; j++) {
        overlapArea += HTMLPolygon.overlapSize(polygons[i], polygons[j])
      }
    }
    return overlapArea
  }
}