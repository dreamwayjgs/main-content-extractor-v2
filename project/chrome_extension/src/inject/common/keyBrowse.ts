import { TASK_NAME_EVALUATION } from "../../common/configs/evaluation"
import { toggleGrids } from "../Extraction/center"
import { elementAtBottom, elementAtLeft, elementAtRight, elementAtTop, elementContains, elementOverlaps } from "../Extraction/domAnalysis"

export function initKeyBrowse() {
  window.onload = () => {
    document.onkeyup = (ev: KeyboardEvent) => {
      if (ev.altKey && ev.key === 'v') {
        // NEXT
        chrome.runtime.sendMessage({
          work: 'common',
          action: 'next'
        })
      }
      if (ev.altKey && ev.key === 'c') {
        // PREV
        chrome.runtime.sendMessage({
          work: 'common',
          action: 'prev'
        })
      }
      if (ev.altKey && ev.key === 'b') {
        // PREV
        chrome.runtime.sendMessage({
          work: 'common',
          action: 'nav'
        })
      }
      if (ev.altKey && ev.key === 'k') {
        // CHECK
        chrome.runtime.sendMessage({
          work: TASK_NAME_EVALUATION,
          action: 'check',
          request: {
            name: 'speedreader',
            isReaderable: true,
            isWorkWell: true,
            content: document.body.firstElementChild
          }
        })
      }
      if (ev.altKey && ev.key === 'n') {
        // CHECK
        chrome.runtime.sendMessage({
          work: TASK_NAME_EVALUATION,
          action: 'check',
          request: {
            name: 'speedreader',
            isReaderable: true,
            isWorkWell: false,
            content: document.body.firstElementChild
          }
        })
      }
      if (ev.altKey && ev.key === 'x') {
        toggleGrids()
      }
      if (ev.altKey && ev.key === 'a') {
        chrome.runtime.sendMessage({
          work: 'curation',
          action: 'checkNoMedia'
        })
      }
      if (ev.altKey && ev.key === 'm') {
        chrome.runtime.sendMessage({
          work: 'curation',
          action: 'checkMedia'
        })
      }
      if (ev.altKey && ev.key === 't') {
        const origin = document.getElementById("origin")!
        const originbaby = document.getElementById("originbaby")!
        const a = document.getElementById("a")!
        const b = document.getElementById("b")!
        const c = document.getElementById("c")!
        const d = document.getElementById("d")!

        console.log("왼")
        console.log(elementAtLeft(origin, a))
        console.log(elementAtLeft(origin, b))
        console.log(elementAtLeft(origin, c))
        console.log(elementAtLeft(origin, d))

        console.log("오")
        console.log(elementAtRight(origin, a))
        console.log(elementAtRight(origin, b))
        console.log(elementAtRight(origin, c))
        console.log(elementAtRight(origin, d))

        console.log("위")
        console.log(elementAtTop(origin, a))
        console.log(elementAtTop(origin, b))
        console.log(elementAtTop(origin, c))
        console.log(elementAtTop(origin, d))

        console.log("아래")
        console.log(elementAtBottom(origin, a))
        console.log(elementAtBottom(origin, b))
        console.log(elementAtBottom(origin, c))
        console.log(elementAtBottom(origin, d))

        console.log("겹칩")
        console.log(elementOverlaps(origin, a))
        console.log(elementOverlaps(origin, b))
        console.log(elementOverlaps(origin, c))
        console.log(elementOverlaps(origin, d))

        console.log("포함")
        console.log(elementContains(origin, a))
        console.log(elementContains(origin, b))
        console.log(elementContains(origin, c))
        console.log(elementContains(origin, d))
        console.log(elementContains(origin, originbaby))
      }
    }
  }
}


