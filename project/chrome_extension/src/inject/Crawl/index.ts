import { inspectPage } from "./inspectPage"

function DOMtoString(document_root: any) {
  var html = '',
    node = document_root.firstChild;
  while (node) {
    switch (node.nodeType) {
      case Node.ELEMENT_NODE:
        html += node.outerHTML;
        break;
      case Node.TEXT_NODE:
        html += node.nodeValue;
        break;
      case Node.CDATA_SECTION_NODE:
        html += '<![CDATA[' + node.nodeValue + ']]>';
        break;
      case Node.COMMENT_NODE:
        html += '<!--' + node.nodeValue + '-->';
        break;
      case Node.DOCUMENT_TYPE_NODE:
        // (X)HTML documents are identified by public identifiers
        html += "<!DOCTYPE " + node.name + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '') + (!node.publicId && node.systemId ? ' SYSTEM' : '') + (node.systemId ? ' "' + node.systemId + '"' : '') + '>\n';
        break;
    }
    node = node.nextSibling;
  }
  return html;
}


export async function initCrawl() {
  console.log("[INIT] Crawl ready")
  chrome.runtime.onMessage.addListener(async (message, sender, sendResponse) => {
    if (chrome.runtime.lastError) {
      sendResponse({
        status: 'error',
        error: chrome.runtime.lastError.message
      })
    }
    const { section, order, id } = message
    if (section === 'crawl' && order === 'preprocess') {
      const elements = await inspectPage()
      const data = JSON.stringify({
        rawHtml: DOMtoString(document),
        elements: elements
      })
      const tempFile = new File([data], `${id}.json`, { type: "text/json;charset=utf-8" })
      const localDataPath = window.URL.createObjectURL(tempFile)
      console.log("Saved", localDataPath)
      sendResponse({
        status: 'ok',
        localDataPath: localDataPath
      })
    }
    return true
  })
  console.log("[INFO] Ready to Crawl")
}