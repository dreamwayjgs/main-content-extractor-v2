import { attachContextMenus } from "./singlePageTests"

export function addTestTools() {
  chrome.contextMenus.create({
    id: "Debugging",
    title: "Debugging Tools"
  })

  chrome.contextMenus.create({
    id: 'GetCurrentTab',
    title: 'Get Current Tab Info',
    onclick: (info, tab) => { console.log("TAB", tab) },
    parentId: "Debugging"
  })

  chrome.contextMenus.create({
    id: 'webNavigationTest',
    title: 'addListener Test',
    parentId: "Debugging",
    onclick: () => {
      chrome.webNavigation.onCommitted.addListener(() => {
        console.log("커밋!")
      })
      chrome.webNavigation.onCompleted.addListener(() => {
        console.log("컴플!")
      })
      chrome.webNavigation.onErrorOccurred.addListener((details: chrome.webNavigation.WebNavigationFramedErrorCallbackDetails) => {
        const frameId = details.frameId
        if (frameId !== 0) return;

        console.log("에러 ㅠㅠ", details.error)
      })
    }
  })


  chrome.contextMenus.create({
    id: 'elemPolygonTest',
    title: 'Polygon test',
    parentId: "Debugging",
    onclick: () => {
      chrome.tabs.query({ active: true, currentWindow: true }, result => {
        const tabId = result[0].id
        if (!tabId) return;
        chrome.tabs.sendMessage(tabId, {
          work: 'test',
          action: 'polygon'
        })
      })
    }
  })

  attachContextMenus()
}