import { LocalConfig } from "../../common";
import { TASK_NAME_EVALUATION } from "../../common/configs/evaluation";
import { requestContentScript } from "../chromePromise";
import { GET } from "../utils";

async function getPid(tab: chrome.tabs.Tab) {
  const url = tab.url
  if (url === undefined) {
    throw new Error('Tab does not have URL')
  }
  const pathname = new URL(url).pathname.replace(/^\/|\/$/g, '');
  const pid = pathname.replace(/\.[^/.]+$/, "")
  return pid
}

async function evaluation(tab: chrome.tabs.Tab, pid: string) {
  const { answers, page } = await GET(LocalConfig.getInstance().host, `evaluation/answer/${pid}`)
  const tabId = tab.id
  const response = await requestContentScript(tabId!, {
    work: TASK_NAME_EVALUATION,
    action: 'evaluate',
    originalUrl: page.url,
    answers: answers,
    options: {
      experiment: true
    }
  })
  console.log(response)
}


export function attachContextMenus() {
  chrome.contextMenus.create({
    id: 'pageTests',
    title: 'Tests on This Page',
    parentId: 'Debugging',
  })

  chrome.contextMenus.create({
    id: 'getPid',
    title: 'Get Pid',
    parentId: 'pageTests',
    onclick: (info, tab) => getPid(tab)
  })

  chrome.contextMenus.create({
    id: 'evaluate',
    title: 'Evaluate',
    parentId: 'pageTests',
    onclick: async (info, tab) => await evaluation(tab, await getPid(tab))
  })
}