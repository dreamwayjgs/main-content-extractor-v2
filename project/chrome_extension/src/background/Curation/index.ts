import { createCurationTask } from './curation'
export { addMenu as addCurationMenu } from './contextMenu'


chrome.runtime.onMessage.addListener((message) => {
  const { request, sourceName } = message
  if (request && request === 'curation') createCurationTask(sourceName)
})