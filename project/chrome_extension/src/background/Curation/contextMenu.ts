import { LocalConfig } from "../../common"
import { getDefaultConfig } from "../../common/configs/common"
import { Page } from "../../types"
import { TaskManager } from "../../types/task"
import { GET } from "../utils"
import { Curation, } from "./curation"

const localConfig = LocalConfig.getInstance()

const createCurationTask = async (info: chrome.contextMenus.OnClickData, tab: chrome.tabs.Tab, sourceName: string) => {
  console.log("START Curation", sourceName)
  const target = await GET(localConfig.host, `curation/source/${sourceName}`) as {
    size: number,
    pages: Page[]
  }
  console.log("Curation Size", target.size)
  const manager = TaskManager.getInstance()
  const curationTask = await manager.create<Curation>(target.pages, Curation, getDefaultConfig())
  curationTask.start()
}

const getSources = async (info: chrome.contextMenus.OnClickData, tab: chrome.tabs.Tab) => {
  const { sources } = await GET(localConfig.host, 'curation/source') as {
    sources: { source: string }[]
  }
  console.log('sources', sources.map(source => source))
  sources.map(v => v.source).forEach(sourceName => {
    chrome.contextMenus.create({
      id: `CurationSource-${sourceName}`,
      title: `Curation - ${sourceName}`,
      onclick: (info, tab) => createCurationTask(info, tab, sourceName),
      parentId: 'CurationSources'
    })
  })
}

export const addMenu = () => {
  chrome.contextMenus.create({
    id: 'CurationMenu',
    title: 'Inspect pages',
  })

  chrome.contextMenus.create({
    id: 'CurationControl',
    title: 'Inspect pages - Control',
  })

  chrome.contextMenus.create({
    id: 'CurationSources',
    title: 'Curation Sources',
    parentId: 'CurationMenu'
  })

  chrome.contextMenus.create({
    id: 'CreateCuration',
    title: 'Get Curation Sources',
    parentId: 'CurationSources',
    onclick: getSources
  })
}