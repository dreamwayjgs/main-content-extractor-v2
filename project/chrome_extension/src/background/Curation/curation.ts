import { find } from "lodash";
import { LocalConfig } from "../../common"
import { Page } from "../../types";
import { Task, TaskManager } from "../../types/task";
import { AnswerTag } from "../../types/models";
import { GET, POST } from "../utils";
import { getSync } from "../../common/storage";
import { getDefaultConfig } from "../../common/configs/common";

const localConfig = LocalConfig.getInstance()

interface DownloadedFile {
  id: string
  downloadId?: number
  filename: string
}

export class Curation extends Task {
  constructor(
    public tabId: number,
    protected pages: Page[],
    private files: DownloadedFile[] = [],
    public defaultTimeout = 10000
  ) {
    super(tabId, pages, defaultTimeout)
    chrome.contextMenus.create({
      id: `CurationPrev-${tabId}`,
      title: `Prev ${tabId}`,
      parentId: 'CurationControl',
      onclick: this.prev
    })
    chrome.contextMenus.create({
      id: `CurationNext-${tabId}`,
      title: `Next ${tabId}`,
      parentId: 'CurationControl',
      onclick: this.next
    })
    chrome.contextMenus.create({
      id: `CurationCheckNoMedia-${tabId}`,
      title: `Check No Media and Next ${tabId}`,
      parentId: 'CurationControl',
      onclick: () => this.tagMedia(false)
    })
    chrome.contextMenus.create({
      id: `CurationCheckMedia-${tabId}`,
      title: `Check Media and Next ${tabId}`,
      parentId: 'CurationControl',
      onclick: () => this.tagMedia(true)
    })
  }
  start() {
    console.log("이 옵션으로 스타트", this.config)
    this.load()
  }
  load() {
    const id = this.currentPage.id
    chrome.tabs.update(this.tabId, { url: `${LocalConfig.getInstance().mhtmlApi}/${id}.mhtml` })
  }
  prev() {
    const prev = this.pageGenerator.next('prev')
    if (!prev.done) {
      this.currentPage = prev.value
      this.load()
    }
  }
  next() {
    const next = this.pageGenerator.next()
    if (next.done) {
      console.log('<<< Curation END >>>')
      this.status = 'finished'
      TaskManager.getInstance().destroy(this.tabId)
    } else {
      this.currentPage = next.value
      this.load()
    }
  }
  async tagMedia(hasMedia: boolean) {
    await POST(localConfig.host, `curation/media/${this.currentPage.id}`, {
      media: hasMedia
    })
    this.next()
  }
  async tag(answer: AnswerTag) {
    const { name, hyu } = answer
    try {
      const userId = await new Promise<string>((resolve, reject) => {
        chrome.storage.sync.get('userId', items => {
          const userId = items.userId
          if (userId === undefined) {
            console.error("UserId NOT SET")
            reject('No UserId')
          }
          resolve(userId)
        })
      })

      const params = {
        tagType: name,
        hyuIndex: hyu,
        userId: userId
      }
      const reqUrl = await POST(localConfig.host, `curation/page/${this.currentPage.id}`, params)
      console.log("Uploaded", reqUrl)
    }
    catch (err) {
      console.error('tagging failed', err)
    }
  }
  attachEvents() {
    chrome.runtime.onMessage.addListener(Curation.addAnswerTag)
    chrome.runtime.onMessage.addListener(Curation.keyBrowse)
  }
  checkEvents() {

  }
  detachEvents() {
    chrome.runtime.onMessage.removeListener(Curation.addAnswerTag)
    chrome.runtime.onMessage.removeListener(Curation.keyBrowse)
  }
  static getTask(tabId: number) {
    const { task, taskType } = TaskManager.getInstance().getTaskByTabId<Curation>(tabId)
    if (taskType === Curation.getClassName()) {
      return task
    }
    return false
  }
  static getTaskWithDownloadId(downloadId: number) {
    const curationTasks = TaskManager.getInstance().getTasksByType<Curation>('Curation')
    return find(curationTasks, task => find(task.files,
      file => file.downloadId === downloadId) !== undefined
    )
  }
  static addAnswerTag(message: any, sender: chrome.runtime.MessageSender, sendResponse: (response: any) => void) {
    const tabId = sender.tab ? sender.tab.id : chrome.tabs.TAB_ID_NONE
    if (tabId === chrome.tabs.TAB_ID_NONE || tabId === undefined) {
      console.warn("알 수 없는 메시지", sender)
      return false
    }
    const { work, action } = message
    const task = Curation.getTask(tabId)
    if (task && work === 'curation' && action === 'tagAnswer') {
      task.tag(message).then(() => {
        sendResponse('OK!')
      })
      return true
    }
  }
  static async keyBrowse(message: any, sender: chrome.runtime.MessageSender, sendResponse: (response: any) => void) {
    if (sender.tab && sender.tab.id) {
      const task = Curation.getTask(sender.tab.id)
      if (task) {
        const { action, work } = message
        if (action === 'next') task.next()
        if (action === 'prev') task.prev()
        if (work === 'curation') {
          if (action === 'checkNoMedia') {
            task.tagMedia(false)
          }
          if (action === 'checkMedia') {
            task.tagMedia(true)
          }
        }
      }
    }
  }
}

export async function createCurationTask(sourceName: string) {
  console.log("START Curation", sourceName)
  const host = await getSync('host')
  const target = await GET(host, `curation/source/${sourceName}`) as {
    size: number,
    pages: Page[]
  }
  console.log("Curation Size", target.size)
  const manager = TaskManager.getInstance()

  const config = JSON.parse(await getSync('configJson'))
  const cf = Array.isArray(config) ? config[0] : config
  const task = await manager.create<Curation>(target.pages, Curation, getDefaultConfig(cf))
  task.start()
}