import { LocalConfig } from "../common"
import { TASK_NAME_CRAWL } from "../common/configs/crawl"
import { TASK_NAME_CURATION } from "../common/configs/curation"
import { TASK_NAME_EVALUATION } from "../common/configs/evaluation"
import { TASK_NAME_EXTRACTION } from "../common/configs/extraction"
import { addContextMenus } from "./ContextMenus"
import { createCrawlTask } from "./Crawl"
import { createExtractionTask } from "./Extraction/Extraction"
import { addTestTools } from "./tools"

LocalConfig.getInstance().init()

addContextMenus()
addTestTools()


chrome.runtime.onMessage.addListener((message) => {
  const { request, sourceName } = message
  console.log("메시지 받음", message)
  if (request) {
    switch (request) {
      case TASK_NAME_CRAWL:
        console.log(`${request} GOGO CRAWL`)
        createCrawlTask(sourceName)
        break
      case TASK_NAME_CURATION:
        break
      case TASK_NAME_EXTRACTION:
        createExtractionTask(sourceName)
        break
      case TASK_NAME_EVALUATION:
        break
      default:
        console.log("알수없는 요청", request)
    }
  }
})