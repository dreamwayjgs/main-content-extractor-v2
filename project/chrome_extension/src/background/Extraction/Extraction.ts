import { Page } from "../../types";
import { Task, TaskManager } from "../../types/task";
import { getRandomInt, LocalConfig } from "../../common";
import { requestContentScript } from "../chromePromise";
import { requestExternalMessage } from "../chromePromise/message";
import { GET, POST, sleep } from "../utils";
import { getSync } from "../../common/storage";
import { ExtractionConfig, getExtractionConfig } from "../../common/configs/extraction";

export class Extraction extends Task {
  base: string
  config: ExtractionConfig
  constructor(
    public tabId: number,
    protected pages: Page[],
    url?: string
  ) {
    super(tabId, pages)
    this.attachControls()
    this.base = url ? url : LocalConfig.getInstance().mhtmlApi
    this.config = getExtractionConfig()
  }
  get mhtmlUrl() {
    return `${this.base}/${this.currentPage.id}.mhtml`
  }
  start() {
    console.log("RUN WITH CONFIGURATIONS:", this.config)
    this.load()
  }
  load() {
    chrome.tabs.update(this.tabId, { url: `${this.base}/${this.currentPage.id}.mhtml` })
  }
  attachEvents() {
    chrome.webNavigation.onCompleted.addListener(Extraction.extractOnLoad)
    chrome.runtime.onMessage.addListener(Extraction.keyBrowse)
    chrome.runtime.onMessage.addListener(Extraction.reportExtraction)
  }
  detachEvents() {
    chrome.webNavigation.onCompleted.removeListener(Extraction.extractOnLoad)
    chrome.runtime.onMessage.removeListener(Extraction.keyBrowse)
    chrome.runtime.onMessage.removeListener(Extraction.reportExtraction)
  }
  static getTask(tabId: number) {
    const { task, taskType } = TaskManager.getInstance().getTaskByTabId<Extraction>(tabId)
    if (taskType === Extraction.getClassName()) return task;
    return undefined
  }
  static async extractOnLoad(details: chrome.webNavigation.WebNavigationFramedCallbackDetails) {
    const frameId = details.frameId
    if (frameId !== 0) return;

    const tabId = details.tabId
    const task = Extraction.getTask(tabId)
    if (task) {
      const { answers } = await GET(LocalConfig.getInstance().host, `extraction/answer/${task.currentPage.id}`)

      interface Report {
        status: 'ok' | 'error' | 'pending'
        data: any
        error: string
        pid: string
      }

      interface Work {
        name: string
        work: (...args: any[]) => Promise<Report>
      }

      const works: Work[] = []

      const mozillaWork: Work = {
        name: 'mozilla',
        work: async () => {
          return await requestContentScript(task.tabId, {
            work: 'extraction',
            action: 'mozilla',
            originalUrl: task.currentPage.url,
            answers: answers,
            pid: task.currentPage.id
          })
        }
      }
      works.push(mozillaWork)


      if (task.config.name.includes('hyucentroid')) {
        const hyucentroidWork: Work = {
          name: task.config.name,
          work: async () => {
            return await requestContentScript(task.tabId, {
              work: 'extraction',
              action: 'centroid',
              originalUrl: task.currentPage.url,
              config: task.config,
              pid: task.currentPage.id
            })
          }
        }
        works.push(hyucentroidWork)
      }


      const boilernetWork: Work = {
        name: 'boilernet',
        work: async () => {
          const boilerNetPrediction = await requestExternalMessage('eeifmhpcckopcaonccnfflgjmelfeegh', {
            work: 'extraction',
            action: 'boilernet',
            pid: task.currentPage.id,
            tabId: task.tabId
          })
          console.log("Boilernet Prediction", Object.keys(boilerNetPrediction).length)
          const boilernetResult = await requestContentScript(task.tabId, {
            work: 'extraction',
            action: 'boilernet',
            prediction: boilerNetPrediction
          })
          return boilernetResult
        }
      }
      works.push(boilernetWork)

      const domDistillerWork: Work = {
        name: 'dom-distiller',
        work: () => {
          return new Promise(resolve => {
            chrome.tabs.executeScript(tabId, { file: "background/domdistiller/domdistiller.js" }, function () {
              chrome.tabs.executeScript(tabId, { file: "background/domdistiller/extract.js" }, result => {
                resolve({
                  status: 'pending',
                  data: '',
                  error: '',
                  pid: task.currentPage.id
                })
              });
            });
          })
        }
      }
      works.push(domDistillerWork)

      const featureExtractionWork: Work = {
        name: 'features',
        work: async () => {
          console.log("answers", answers.map((a: any) => a.hyuIndex))
          return await requestContentScript(task.tabId, {
            work: 'extraction',
            action: 'features',
            answers: answers
          })
        }
      }
      works.push(featureExtractionWork)

      const tabNetExtractionWork: Work = {
        name: 'tabnet',
        work: async () => {
          const { positives } = await GET(LocalConfig.getInstance().host, `extraction/predict/${task.currentPage.id}`)
          console.log("tabnet positives", positives.length)
          return await requestContentScript(task.tabId, {
            work: 'extraction',
            action: 'tabnet',
            positives: positives
          })
        }
      }

      works.push(tabNetExtractionWork)

      const documentFeaturesExtractionWork: Work = {
        name: 'docFeatures',
        work: async () => {
          return await requestContentScript(task.tabId, {
            work: 'extraction',
            action: 'docFeatures'
          })
        }
      }
      works.push(documentFeaturesExtractionWork)

      console.log("Start work", works, task.config)

      const jobs = works.filter(w => task.config.name.includes(w.name))

      console.info("TODO JOBS", jobs)

      const sendReports = jobs.map(async ({ name, work }) => {
        const { status, data, error, pid } = await work()
        console.info('extraction', pid, name, status)
        if (!task.config.watch) {
          if (status === 'ok') {
            return POST(LocalConfig.getInstance().host, `extraction/result/${name}/${pid}`, data)
          } else if (status === 'error') {
            // TODO: Report server error
            return POST(LocalConfig.getInstance().host, `extraction/error/${name}/${pid}`, error)
          }
        }
        return true
      })

      Promise.all(sendReports).catch(() => {
        console.error("POST 중 에러", task.config.name, task.currentPage.id)
      })
      if (task.config.auto) {
        console.info("DONE --> NEXT", task.tabId, task.currentPage.id)
        await sleep(100 + getRandomInt(1000))
        task.next()
      }
    }
  }
  static async reportExtraction(message: any, sender: chrome.runtime.MessageSender) {
    if (chrome.runtime.lastError) {
      console.error("여기냐? 리포팅")
    }
    console.log("Pending job done", message)
    if (sender.tab && sender.tab.id) {
      const task = Extraction.getTask(sender.tab.id)
      if (task) {
        const { work, name, data, status, error } = message
        if (!task.config.watch) {
          if (work === 'extraction' && status === 'done') {
            const pid = task.currentPage.id
            console.info("늦은전송", pid)
            await POST(LocalConfig.getInstance().host, `extraction/result/${name}/${pid}`, data)
          }
        }
      }
    }
  }
  static async keyBrowse(message: any, sender: chrome.runtime.MessageSender, sendResponse: (response: any) => void) {
    if (chrome.runtime.lastError) {
      console.error("Error on browsing with the keyboard", chrome.runtime.lastError)
    }
    if (sender.tab && sender.tab.id) {
      const task = Extraction.getTask(sender.tab.id)
      if (task) {
        const { action, request } = message
        if (action === 'next') task.next()
        if (action === 'prev') task.prev()
      }
    }
  }
}

export async function createExtractionTask(sourceName: string) {
  console.log("START Extraction", sourceName)
  const host = await getSync('host')
  const target = await GET(host, `extraction/source/${sourceName}`) as {
    size: number,
    pages: Page[]
  }
  console.log("Extraction Size", target.size)
  const manager = TaskManager.getInstance()

  const config = JSON.parse(await getSync('configJson'))
  const cfs = Array.isArray(config) ? config.map(cf => getExtractionConfig(cf)) : [getExtractionConfig(config)]
  const task = await manager.create<Extraction>(target.pages, Extraction, cfs)
  task.start()
}