import { Page } from "../../types";
import { TaskManager } from "../../types/task";
import { LocalConfig } from "../../common";
import { GET } from "../utils";
import { Extraction } from "./Extraction";
import { ExtractionConfig, getExtractionConfig } from "../../common/configs/extraction";
import { getConfigInput } from "../../common/configs/common";

const localConfig = LocalConfig.getInstance()

const createExtractionTask = async (info: chrome.contextMenus.OnClickData, tab: chrome.tabs.Tab, sourceName: string, config?: Partial<ExtractionConfig>) => {
  console.log("START Extraction", sourceName)
  if (sourceName === 'all') {
    const sourcesStr = 'GoogleTrends-2020,GoogleTrends-2017,GoogleTrends-2020-KR,GoogleTrends-2020-JP,GoogleTrends-2020-ID,baidu-2020,GoogleTrends-2020-SA,GoogleTrends-2020-FR,GoogleTrends-2020-RU'
    const sources = sourcesStr.split(',')
    const pages: Page[] = []
    for (const source of sources) {
      const target = await GET(localConfig.host, `evaluation/source/${source}`, { answerOnly: 'true' }) as {
        size: number,
        pages: Page[]
      }
      pages.push(...target.pages)
    }

    const manager = TaskManager.getInstance()
    // Hyucentroid Example
    // manager.enqueue(pages, Extraction, getExtractionConfig({
    //   watch: false,
    //   auto: true,
    //   gridRow: 7,
    //   gridColumn: 8,
    //   gridCoef: 2,
    //   linkRatio: 0.5,
    //   reversed: true,
    //   widthIncrease: 1.7
    // }))
    manager.enqueue(pages, Extraction, getExtractionConfig({
      watch: true,
      auto: false,
      name: 'tabnet'
    }))
    manager.thread = 1
    await manager.runQueue()

  } else {
    const target = await GET(localConfig.host, `extraction/source/${sourceName}`) as {
      size: number,
      pages: Page[]
    }
    console.log("Extraction Size", target.size)
    const manager = TaskManager.getInstance()
    const runConfig = getExtractionConfig(config)
    const extractionTask = await manager.create<Extraction>(target.pages, Extraction, runConfig)
    extractionTask.start()
  }
}

export const addMenu = () => {
  chrome.contextMenus.create({
    id: 'ExtractionSource',
    title: `Extraction Source`,
  })
  chrome.contextMenus.create({
    id: 'ExtractionControl',
    title: `Extraction Control`,
  })
  chrome.contextMenus.create({
    id: 'ExtractionSourceAuto',
    title: `Extraction Source Auto`,
    parentId: 'ExtractionSource'
  })
  chrome.contextMenus.create({
    id: 'ExtractionSourceManual',
    title: `Extraction Source Manual`,
    parentId: 'ExtractionSource'
  })
  chrome.contextMenus.create({
    id: 'ExtractionSourceWatch',
    title: `Extraction Source Watch`,
    parentId: 'ExtractionSource'
  })

  chrome.contextMenus.create({
    id: 'ExtractionGetSources',
    title: 'Get Sources',
    parentId: 'ExtractionSource',
    onclick: async () => {
      const { sources } = await GET(localConfig.host, 'extraction/source') as {
        sources: { source: string }[]
      }
      console.log('sources', sources.map(source => source))
      sources.map(v => v.source).forEach(sourceName => {
        chrome.contextMenus.create({
          id: `ExtractionSourceManual-${sourceName}`,
          title: `Extraction - ${sourceName}`,
          onclick: (info, tab) => createExtractionTask(info, tab, sourceName, { watch: false }),
          parentId: 'ExtractionSourceManual'
        })
      })
      sources.map(v => v.source).forEach(sourceName => {
        chrome.contextMenus.create({
          id: `ExtractionSourceAuto-${sourceName}`,
          title: `Extraction - ${sourceName}`,
          onclick: (info, tab) => createExtractionTask(info, tab, sourceName, { auto: true }),
          parentId: 'ExtractionSourceAuto'
        })
      })
      sources.map(v => v.source).forEach(sourceName => {
        chrome.contextMenus.create({
          id: `ExtractionSourceWatch-${sourceName}`,
          title: `Extraction - ${sourceName}`,
          onclick: (info, tab) => createExtractionTask(info, tab, sourceName, { auto: false, watch: true }),
          parentId: 'ExtractionSourceWatch'
        })
      })
    }
  })

  chrome.contextMenus.create({
    id: `ExtractionSourceAuto-all`,
    title: `Extraction - all`,
    onclick: (info, tab) => createExtractionTask(info, tab, 'all', { auto: true, watch: false }),
    parentId: 'ExtractionSourceAuto'
  })

  chrome.contextMenus.create({
    id: 'ExtractionNow',
    title: `Extraction Current Page`,
    parentId: 'ExtractionSource',
    onclick: (info, tab) => {
      const tabId = tab.id

      console.log("Extraction Current page with", tab.id)
      chrome.tabs.sendMessage(tabId!, {
        work: 'extraction',
        action: 'centroid',
        config: getExtractionConfig()
      }, response => {
        console.log("Result: ")
        console.log(response)
      })
      // getConfigInput().then((config) => {
      //   const cf = JSON.parse(config)
      //   console.log("Extraction Current page with", cf)

      // }).catch(e => {
      //   console.log("getconfiginput", e)
      // })
    }
  })
}