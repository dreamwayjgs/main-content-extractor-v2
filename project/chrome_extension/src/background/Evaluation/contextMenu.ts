import { Page } from "../../types"
import { TaskManager } from "../../types/task"
import { LocalConfig } from "../../common"
import { GET } from "../utils"
import { Evaluation } from "./Evaluation"
import { EvaluationConfig, getEvaluationConfig } from "../../common/configs/evaluation"

const localConfig = LocalConfig.getInstance()

const createEvaluationTask = async (
  info: chrome.contextMenus.OnClickData, tab: chrome.tabs.Tab, sourceName: string, options?: Partial<EvaluationConfig>) => {
  console.log("START Evaluation", sourceName)
  if (sourceName === 'all') {
    const sourcesStr = 'GoogleTrends-2020,GoogleTrends-2017,GoogleTrends-2020-KR,GoogleTrends-2020-JP,GoogleTrends-2020-ID,baidu-2020,GoogleTrends-2020-SA,GoogleTrends-2020-FR,GoogleTrends-2020-RU'
    const sources = sourcesStr.split(',')
    console.log("RUN ALL", sources)
    const pages: Page[] = []
    for (const source of sources) {
      const target = await GET(localConfig.host, `evaluation/source/${source}`, { answerOnly: 'true' }) as {
        size: number,
        pages: Page[]
      }
      pages.push(...target.pages)
    }
    const manager = TaskManager.getInstance()
    const runConfig = getEvaluationConfig(options)

    manager.enqueue(pages, Evaluation, runConfig)
    await manager.runQueue()
  } else {
    const target = await GET(localConfig.host, `evaluation/source/${sourceName}`) as {
      size: number,
      pages: Page[]
    }
    console.log("Evaluation Size", target.size)
    const manager = TaskManager.getInstance()
    const runConfig = getEvaluationConfig(options)
    const evaluationTask = await manager.create<Evaluation>(target.pages, Evaluation, runConfig)
    evaluationTask.start()
  }
}

export const addMenu = () => {
  chrome.contextMenus.create({
    id: 'EvaluationSource',
    title: `Evaluation Source`,
  })
  chrome.contextMenus.create({
    id: 'EvaluationControl',
    title: `Evaluation Control`,
  })
  chrome.contextMenus.create({
    id: 'EvaluationSourceManual',
    title: `Evaluation Source Manual`,
    parentId: 'EvaluationSource'
  })
  chrome.contextMenus.create({
    id: 'EvaluationSourceAuto',
    title: `Evaluation Source Auto`,
    parentId: 'EvaluationSource'
  })
  chrome.contextMenus.create({
    id: 'EvaluationSourceOrigin',
    title: `Evaluation Source Origin`,
    parentId: 'EvaluationSource'
  })
  chrome.contextMenus.create({
    id: 'EvaluationSourceExperiment',
    title: `Evaluation Source Experiment`,
    parentId: 'EvaluationSource'
  })

  chrome.contextMenus.create({
    id: 'EvaluationGetSource',
    title: 'Get Sources',
    parentId: 'EvaluationSource',
    onclick: async () => {
      const { sources } = await GET(localConfig.host, 'evaluation/source') as {
        sources: { source: string }[]
      }
      console.log('sources', sources)
      console.log('sources', sources.map(s => s.source).join(','))
      sources.map(v => v.source).forEach(sourceName => {
        chrome.contextMenus.create({
          id: `EvaluationSource-${sourceName}`,
          title: `Evaluation - ${sourceName}`,
          onclick: (info, tab) => createEvaluationTask(info, tab, sourceName, { watch: true }),
          parentId: 'EvaluationSourceManual'
        })
        chrome.contextMenus.create({
          id: `EvaluationSourceAuto-${sourceName}`,
          title: `EvaluationAuto - ${sourceName}`,
          onclick: (info, tab) => createEvaluationTask(info, tab, sourceName, { auto: true }),
          parentId: 'EvaluationSourceAuto'
        })
        chrome.contextMenus.create({
          id: `EvaluationSourceOrigin-${sourceName}`,
          title: `EvaluationOrigin - ${sourceName}`,
          onclick: (info, tab) => createEvaluationTask(info, tab, sourceName, { openOrigin: true }),
          parentId: 'EvaluationSourceOrigin'
        })
        chrome.contextMenus.create({
          id: `EvaluationSourceExperiment-${sourceName}`,
          title: `EvaluationExperiment - ${sourceName}`,
          onclick: (info, tab) => createEvaluationTask(info, tab, sourceName, { experiment: true, auto: false, watch: false }),
          parentId: 'EvaluationSourceExperiment'
        })
      })
    }
  })
  chrome.contextMenus.create({
    id: `EvaluationSourceExperiment-all`,
    title: `EvaluationExperiment - all`,
    onclick: (info, tab) => createEvaluationTask(info, tab, 'all', { experiment: true, auto: false, watch: false }),
    parentId: 'EvaluationSourceExperiment'
  })
  chrome.contextMenus.create({
    id: `EvaluationSourceAuto-all`,
    title: `EvaluationAuto - all`,
    onclick: (info, tab) => createEvaluationTask(info, tab, 'all', { auto: true, watch: false }),
    parentId: 'EvaluationSourceAuto'
  })
}