import { filter, find } from "lodash"
import { Page } from "../../types"
import { Task, TaskManager } from "../../types/task"
import { LocalConfig } from "../../common"
import { requestContentScript } from "../chromePromise"
import { GET, POST } from "../utils"
import { getSync } from "../../common/storage"
import { EvaluationConfig, getEvaluationConfig, TASK_NAME_EVALUATION } from "../../common/configs/evaluation"

export class Evaluation extends Task {
  base: string
  config: EvaluationConfig
  constructor(
    public tabId: number,
    protected pages: Page[],
    url?: string
  ) {
    super(tabId, pages)
    this.attachControls()
    this.base = url ? url : LocalConfig.getInstance().mhtmlApi
    this.config = getEvaluationConfig()
  }
  get answerUrl() {
    return `${this.base}/${this.currentPage.id}.mhtml`
  }
  start() {
    console.log("RUN WITH CONFIGURATIONS:", this.config)
    this.load()
  }
  load() {
    if (this.config.openOrigin) chrome.tabs.update(this.tabId, { url: this.currentPage.url });
    else chrome.tabs.update(this.tabId, { url: `${this.base}/${this.currentPage.id}.mhtml` });
  }
  check(message: any) { }
  attachEvents() {
    console.log(`Attach ${TASK_NAME_EVALUATION} Events on ${this.tabId}`)
    chrome.webNavigation.onCompleted.addListener(Evaluation.evalOnLoad)
    chrome.runtime.onMessage.addListener(Evaluation.keyBrowse)
  }
  detachEvents() {
    console.log(`Detach ${TASK_NAME_EVALUATION} Events on ${this.tabId}`)
    chrome.webNavigation.onCompleted.removeListener(Evaluation.evalOnLoad)
    chrome.runtime.onMessage.removeListener(Evaluation.keyBrowse)
  }
  static getTask(tabId: number) {
    const { task, taskType } = TaskManager.getInstance().getTaskByTabId<Evaluation>(tabId)
    if (taskType === Evaluation.getClassName()) {
      return task
    }
    return undefined
  }
  static async evalOnLoad(details: chrome.webNavigation.WebNavigationFramedCallbackDetails) {
    const frameId = details.frameId
    if (frameId !== 0) return;

    const tabId = details.tabId
    const task = Evaluation.getTask(tabId)
    if (task) {
      const { answers } = await GET(LocalConfig.getInstance().host, `evaluation/answer/${task.currentPage.id}`)

      const { results } = await GET(LocalConfig.getInstance().host, `evaluation/extraction/${task.currentPage.id}`)//, { name: task.config[0].name, ...task.config[0].query })
      const extractionResults = (results as any[]).filter(result => result.name.includes('tabnet')).map(result => ({ ...result, name: result.name + '_2' }))

      const response = await requestContentScript(task.tabId, {
        work: 'evaluation',
        action: 'evaluate',
        originalUrl: task.currentPage.url,
        answers: answers,
        results: extractionResults,
        config: task.config,
        pid: task.currentPage.id
      })

      const { status, data, error, pid } = response
      if (!task.config.watch && task.config.experiment) {
        const center = find(data, e => e.name === 'center')
        if (!!center) {
          await POST(LocalConfig.getInstance().host, `evaluation/experiments/center/${pid}`, center)
        }
        const nav = filter(data, e => e.name === 'nav')
        if (nav.length > 0) {
          for (const item of nav) { // Need to be seqeuntial
            await POST(LocalConfig.getInstance().host, `evaluation/experiments/nav/${pid}`, item)
          }
        }
      }
      else if (!task.config.watch && status === 'ok') {
        await POST(LocalConfig.getInstance().host, `evaluation/metrics/${pid}`, data)
      } else if (task.config.watch) {
        console.log(`Watching mode: ${TASK_NAME_EVALUATION} POST skipped ${pid}`)
      }
      else {
        console.error("Eval Status", status)
        console.error(error)
      }
      if (task.config.auto) {
        task.next()
      }
    }
  }
  static async keyBrowse(message: any, sender: chrome.runtime.MessageSender) {
    console.log(sender.tab)
    if (sender.tab && sender.tab.id) {
      const task = Evaluation.getTask(sender.tab.id)
      if (task) {
        const { action, request } = message
        if (action === 'next') task.next()
        if (action === 'prev') task.prev()
        if (action === 'check') task.check(request)
        if (action === 'nav') {
          const { answers } = await GET(LocalConfig.getInstance().host, `evaluation/answer/${task.currentPage.id}`)
          const response = await requestContentScript(task.tabId, {
            work: TASK_NAME_EVALUATION,
            action: 'evaluate',
            originalUrl: task.currentPage.url,
            answers: answers,
            options: task.config
          })
          const { status, data, error } = response
          if (!task.config.watch && task.config.experiment) {
            console.log("Experimental mode", data)
          }
        }
      }
    }
  }
}

export async function createEvaluationTask(sourceName: string) {
  console.log("START evaluation", sourceName)
  const host = await getSync('host')
  const target = await GET(host, `evaluation/source/${sourceName}`) as {
    size: number,
    pages: Page[]
  }
  console.log("Evaluation Size", target.size)
  const manager = TaskManager.getInstance()

  const config = JSON.parse(await getSync('configJson'))
  const cfs = Array.isArray(config) ? config.map(cf => getEvaluationConfig(cf)) : [getEvaluationConfig(config)]
  const task = await manager.create<Evaluation>(target.pages, Evaluation, cfs)
  task.start()
}