import { TASK_NAME_EVALUATION } from '../../common/configs/evaluation'
import { createEvaluationTask } from './Evaluation'

export { addMenu as addEvaluationMenu } from './contextMenu'


chrome.runtime.onMessage.addListener((message) => {
  const { request, sourceName } = message
  console.log("Message", TASK_NAME_EVALUATION, message)
  if (request && request === TASK_NAME_EVALUATION) createEvaluationTask(sourceName)
})