
import { findIndex } from "lodash"
import { LocalConfig } from "../../common"
import { requestContentScript } from "../chromePromise"
import { GET, POST_Form } from "../../common/api"
import { Crawler } from "./Crawler"

const localConfig = LocalConfig.getInstance()

const crawlCurrentPage = async (info: chrome.contextMenus.OnClickData, tab: chrome.tabs.Tab) => {
  const tabId = tab.id
  if (tabId === undefined || tabId === chrome.tabs.TAB_ID_NONE || tab.url === undefined) {
    alert("저장할 수 없는 페이지입니다")
    return
  }
  const customId = 'washington'
  try {
    const { status, localDataPath, error } = await requestContentScript(tabId, {
      section: 'crawl',
      order: 'preprocess',
      id: customId
    })
    const { rawHtml, elements } = await (await fetch(localDataPath)).json()

    const index = findIndex(elements, { '_node_name': 'HTML' })
    const htmlEl = elements[index]
    console.log("Single Crawl Ended", htmlEl._bottom, htmlEl._right)

    // const data = {
    //   id: customId,
    //   url: tab.url,
    //   rawHtml: rawHtml,
    //   elements: JSON.stringify(elements),
    //   mhtml: await pageCapture(tabId)
    // }
    // await POST_Form(localConfig.host, 'crawl', data)
  }
  catch (err) {
    console.log("cannot crawled")
  }
}

export const createCrawlTask = async (sourceName: string) => {
  console.log("START Crawl", sourceName)
  const targets = await GET(localConfig.host, `crawl/source/${sourceName}`) as {
    size: number,
    pages: any[]
  }
  console.log("Crawl size", targets.size)
  const crawler = await Crawler.create(targets.pages)
  await crawler.startOnNewTab()
}

const getSources = async (info: chrome.contextMenus.OnClickData, tab: chrome.tabs.Tab) => {
  const { sources } = await GET(localConfig.host, 'crawl/source') as {
    sources: { source: string }[]
  }
  console.log('sources', sources.map(source => source))
  sources.map(v => v.source).forEach(sourceName => {
    chrome.contextMenus.create({
      id: `CrawlSource-${sourceName}`,
      title: `Crawl - ${sourceName}`,
      onclick: () => createCrawlTask(sourceName),
      parentId: 'CrawlMenu'
    })
  })
}


export const addCrawlMenu = () => {
  chrome.contextMenus.create({
    id: 'CrawlMenu',
    title: 'Saving pages'
  })

  chrome.contextMenus.create({
    id: 'CrawlCurrentPage',
    title: 'Save current page in MHtml',
    onclick: crawlCurrentPage,
    parentId: 'CrawlMenu'
  })

  chrome.contextMenus.create({
    id: 'GetCrawlSources',
    title: 'Get Crawl Sources and Pages',
    onclick: getSources,
    parentId: 'CrawlMenu'
  })
}

