import { Page } from "../../types"
import { LocalConfig } from "../../common"
import { requestContentScript } from "../chromePromise"
import { POST_Form } from "../../common/api"
import { pageCapture } from "./pageCapture"
import { CrawlConfig, TASK_NAME_CRAWL, getCrawlConfig } from "../../common/configs/crawl"
import { Task } from "../../types/task"

// const MAX_LOAD_TIMEOUT = 12000
// const EXTRA_TIME_AFTER_LOADED = 5000

export class Crawler extends Task {
  private runner: Generator<Page, void, unknown>
  private cursor: Page
  public config: CrawlConfig
  constructor(
    private tab: chrome.tabs.Tab,
    public tabId: number,
    public pages: Page[],
    private loadTimedOut = false,
    private loadTimeoutId?: NodeJS.Timeout,
  ) {
    super(tabId, pages)
    this.runner = this.run()
    const start = this.runner.next()
    this.config = getCrawlConfig()
    console.log(`Crawl config: ${JSON.stringify(this.config)}`)
    if (!start.done) this.cursor = start.value
    else throw Error("Page length is 1")
  }
  static create(pages: Page[]) {
    return new Promise<Crawler>((resolve, reject) => {
      chrome.tabs.create({ active: true }, tab => {
        if (chrome.runtime.lastError) {
          reject(new Error(chrome.runtime.lastError.message))
          return
        }
        resolve(new Crawler(tab, tab.id!, pages))
      })
    })
  }
  async startOnNewTab() {
    chrome.webNavigation.onCommitted.addListener(this.requestOnTimeout)
    chrome.webNavigation.onCompleted.addListener(this.requestOnComplete)
    chrome.webNavigation.onErrorOccurred.addListener(this.skipFaultyPage)
    await this.open(this.cursor.url)
  }

  *run() {
    for (const [index, page] of Object.entries(this.pages)) {
      console.info(`Progress: ${index} / ${this.pages.length}`)
      yield page
    }

    console.log("Event detach", chrome.webNavigation.onCommitted.hasListener(this.requestOnTimeout))

    chrome.webNavigation.onCommitted.removeListener(this.requestOnTimeout)
    chrome.webNavigation.onCompleted.removeListener(this.requestOnComplete)
    chrome.webNavigation.onErrorOccurred.removeListener(this.skipFaultyPage)
    console.info("CRAWL FINISHED")
  }

  detachEvents() {
    console.log("Crawl force stop", this.tabId)
    chrome.webNavigation.onCommitted.removeListener(this.requestOnTimeout)
    chrome.webNavigation.onCompleted.removeListener(this.requestOnComplete)
    chrome.webNavigation.onErrorOccurred.removeListener(this.skipFaultyPage)
    console.info("CRAWL FINISHED")
  }

  async next() {
    const next = this.runner.next()
    if (!next.done) {
      this.cursor = next.value
      await this.open(this.cursor.url)
    }
  }

  open(url: string) {
    return new Promise<void>((resolve, reject) => {
      if (chrome.runtime.lastError) {
        reject(new Error(chrome.runtime.lastError.message))
      }
      console.info(`Moving... ${url}`)
      chrome.tabs.update(this.tabId, { url: url }, () => {
        console.info(`Moved ${url}`)
        resolve()
      })
    })
  }

  skipFaultyPage = async (details: chrome.webNavigation.WebNavigationFramedErrorCallbackDetails) => {
    const frameId = details.frameId
    if (frameId !== 0) return;

    console.log("Error on ", details.tabId)
    console.log("by", details.error)
    const ignorableErrorList = ['net::ERR_ABORTED']
    if (!ignorableErrorList.includes(details.error)) {
      if (this.loadTimeoutId) clearTimeout(this.loadTimeoutId);
      await this.next()
    }
  }

  requestOnTimeout = async (details: chrome.webNavigation.WebNavigationFramedCallbackDetails) => {
    const frameId = details.frameId
    if (frameId !== 0) return;

    this.loadTimeoutId = setTimeout(async () => {
      this.loadTimedOut = true
      console.warn("CRAWL: Something wrong but try anyway")
      await this.crawl()
    }, this.config.timeoutAfterOpen)
  }

  requestOnComplete = async (details: chrome.webNavigation.WebNavigationFramedCallbackDetails) => {
    const frameId = details.frameId
    if (frameId !== 0) return;

    console.info("CRAWL: LOAD EVENT fired - Good TO GO / waiting...", this.loadTimeoutId)
    if (this.loadTimeoutId) clearTimeout(this.loadTimeoutId);
    setTimeout(async () => {
      console.info("CRAWL!")
      await this.crawl()
    }, this.config.timeoutAfterLoad)
  }

  crawl = async () => {
    console.log("CRAWL: Crawling...", this.tabId, this.cursor.url)
    try {
      const response = await requestContentScript(this.tabId, {
        section: TASK_NAME_CRAWL,
        order: 'preprocess',
        id: this.cursor.id
      })
      const { status, localDataPath, error } = response

      console.log("Crawl status", status, localDataPath, error)

      if (status !== 'ok') {
        console.log("콘텐트 스크립트 응답 에러", error)
        throw Error(error);
      }
      const { rawHtml, elements } = await (await fetch(localDataPath)).json()
      const data = {
        id: this.cursor.id,
        url: this.cursor.url,
        rawHtml: rawHtml,
        elements: JSON.stringify(elements),
        isCompleted: this.loadTimedOut,
        mhtml: await pageCapture(this.tabId)
      }

      if (!this.config.watch) await POST_Form(LocalConfig.getInstance().host, 'crawl', data);
      if (this.config.auto) await this.next()
    }
    catch (err) {
      console.error("수집 실패", err)
      if (this.config.auto) await this.next()
    }
  }
}

// export async function createCrawlTask(sourceName: string) {
//   console.log("START Crawl", sourceName)
//   const host = await getSync('host')
//   const target = await GET(host, `curation/source/${sourceName}`) as {
//     size: number,
//     pages: Page[]
//   }
//   console.log("Curation Size", target.size)
//   const manager = TaskManager.getInstance()

//   const config = JSON.parse(await getSync('configJson'))
//   const cf = Array.isArray(config) ? config[0] : config
//   const task = await manager.create<Crawler>(target.pages, Crawler, getDefaultConfig(cf))
//   task.start()
// }