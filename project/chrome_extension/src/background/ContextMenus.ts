import { LocalConfig } from "../common"
import { addCrawlMenu } from "./Crawl"
import { addCurationMenu } from "./Curation"
import { addEvaluationMenu } from "./Evaluation"
import { addExtractionMenu } from "./Extraction"

export function addContextMenus() {
  addCrawlMenu()
  addCurationMenu()
  addExtractionMenu()
  addEvaluationMenu()
  chrome.contextMenus.create({
    id: 'ResetEnv',
    title: 'Reset env',
    onclick: () => {
      const localConfig = LocalConfig.getInstance()
      localConfig.setToEnv()
    }
  })
}