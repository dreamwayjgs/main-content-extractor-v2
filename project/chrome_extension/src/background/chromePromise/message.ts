export const requestContentScript = async (tabId: number, message: any): Promise<any> => {
  console.log("[MSG] 리퀘스트 콘텐트스크립트")
  return new Promise<any>((resolve, reject) => {
    chrome.tabs.sendMessage(tabId, message, response => {
      if (chrome.runtime.lastError) {
        reject(chrome.runtime.lastError.message)
        return
      }
      resolve(response)
    })
  })
}

export const requestExternalMessage = async (extId: string, message: any): Promise<any> => {
  console.log("[MSG] 리퀘스트 백그라운드 스크립트")
  return new Promise<any>((resolve, reject) => {
    chrome.runtime.sendMessage(extId, message, response => {
      if (chrome.runtime.lastError) {
        reject(chrome.runtime.lastError.message)
        return
      }
      resolve(response)
    })
  })
}