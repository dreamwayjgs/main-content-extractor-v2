import { getSync } from "../common/storage"
import { Source } from "../types/models"

async function run() {
  // 현재 소스를 체크
  const sources = await getSync('sources') as Source[]
  console.log('소스?', sources)
  const sourceNames = sources.map(v => v.source)
  console.log("이거를 크롤링해라", sourceNames)
}

export async function crawlAll() {
  run()
}