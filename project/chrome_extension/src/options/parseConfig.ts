import { setStatus } from "./status"
import { load } from 'js-yaml'
import { TaskConfig, getDefaultConfig } from "../common/configs/common"
import { CrawlConfig, TASK_NAME_CRAWL, getCrawlConfig } from "../common/configs/crawl"
import { TASK_NAME_CURATION } from "../common/configs/curation"
import { TASK_NAME_EXTRACTION } from "../common/configs/extraction"
import { TASK_NAME_EVALUATION } from "../common/configs/evaluation"

export function parseConfig(textInputEl: HTMLTextAreaElement, parseResultEl: HTMLElement) {
  const text = textInputEl.value
  try {
    if (text.length === 0) {
      parseResultEl.innerText = 'Empty field'
    }
    const jsonValue: any = load(text)
    for (const [key, value] of Object.entries(jsonValue)) {
      if (key === TASK_NAME_CRAWL) parseCrawlConfig(jsonValue);
      else if (key === TASK_NAME_CURATION) parseCrawlConfig(jsonValue);
      else if (key === TASK_NAME_EXTRACTION) parseCrawlConfig(jsonValue);
      else if (key === TASK_NAME_EVALUATION) parseCrawlConfig(jsonValue);
      else parseCommonConfig(jsonValue);

    }
    setStatus(new Promise<void>((resolve, reject) => {
      chrome.storage.sync.set({
        'configText': textInputEl.value,
        'configJson': JSON.stringify(jsonValue)
      }, () => {
        parseResultEl.textContent = JSON.stringify(jsonValue, undefined, 2)
        resolve()
      })
    }), {
      loading: 'Saving...',
      done: 'Saved!'
    })
  }
  catch (e) {
    console.error("에러?", e)
    if (e instanceof SyntaxError) {
      parseResultEl.innerText = 'No valid YAML string'
    }
  }
}


function parseCommonConfig(config: any): TaskConfig {
  const actionConfig = getDefaultConfig(config)
  console.log("General config", actionConfig)
  return actionConfig
}

function parseCrawlConfig(config: any): CrawlConfig {
  const crawlConfig = getCrawlConfig(config)
  console.log("Crawl config", crawlConfig)
  return crawlConfig
}