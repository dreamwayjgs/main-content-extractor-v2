import { getSync } from "../common/storage"
import { Source } from "../types/models"
import { generateLinks } from "./backgroundConnection"

async function insertExecutables() {
  // 현재 소스를 체크
  const sources = await getSync('sources') as Source[]
  console.log('소스?', sources)
  const sourceNames = sources.map(v => v.source)
  console.log("이거를 크롤링해라", sourceNames)

  sources.map((source) => generateLinks({
    sourceName: source.source,
    request: 'curation',
    text: `Curation - ${source.source}`
  }))
}

export async function curationAll() {
  insertExecutables()
}