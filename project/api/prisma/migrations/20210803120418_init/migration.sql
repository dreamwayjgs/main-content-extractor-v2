/*
  Warnings:

  - Changed the type of `elements` on the `Stored` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "Stored" DROP COLUMN "elements",
ADD COLUMN     "elements" JSONB NOT NULL;
