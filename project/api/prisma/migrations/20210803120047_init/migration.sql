-- CreateTable
CREATE TABLE "Stored" (
    "id" BIGSERIAL NOT NULL,
    "elements" JSONB[],

    PRIMARY KEY ("id")
);
