import { appendFile } from "fs";
import { Context, Next } from "koa";
import { cloneDeep, cloneDeepWith, find, uniq } from "lodash";
import fetch from "node-fetch";
import { mongo } from "../app";
import { parseQuery } from "../utils";

interface ElementFeatures {
  basicFeatures: (string | number)[][]
  answerTagged: string[]
  distanceToCenters: number[][]
}

interface DocumentFeatures {
  documentFeatures: {
    screen: number[],
    document: number[]
  }
}
export async function postExtractResult(ctx: Context) {
  const { name, pid } = ctx.params
  const result = ctx.request.body
  const test = !true
  let dup = false

  if (test)
    console.log(name, pid, Object.keys(result))

  const existed = await mongo.extractionResult.findFirst({
    where: {
      pid: pid,
      name: name
    }
  })

  const existedFeature = await mongo.elementFeatures.findFirst({
    where: {
      pid: pid
    }
  })

  if (!test && existed === null && name !== 'features' && name !== 'docFeatures') {
    console.log("New Data", name, pid)
    await mongo.extractionResult.create({
      data: {
        pid: pid,
        name: name,
        result: result
      }
    })
  } else if (!test && existedFeature === null && name === 'features') {
    const { basicFeatures, answerTagged, distanceToCenters } = result as ElementFeatures

    const htmlTags = cloneDeep(await mongo.htmlTag.findMany()).map(o => ({ num: o.num, name: o.name }))
    const count = htmlTags.length

    const quantify = (feat: any[]): number[] => {
      const result = [...feat].map(v => v !== null ? v : 0)
      result[0] = parseInt(feat[0] as string)

      const tag = (feat[1] as string).toLowerCase()
      const existTag = find(htmlTags, o => o.name === tag)

      if (existTag) {
        result[1] = existTag.num
      } else {
        result[1] = htmlTags.length
        htmlTags.push({ num: htmlTags.length, name: tag })
      }

      result[5] = result[5] ? 1 : 0
      return result
    }
    const basicFeaturesQuantified = basicFeatures.map(quantify)
    const jobs = []
    for (let i = 0; i < basicFeatures.length; i++) {
      const job = mongo.elementFeatures.create({
        data: {
          pid: pid,
          basicFeatures: basicFeaturesQuantified[i],
          answer: answerTagged[i],
          distanceToCenters: distanceToCenters[i]
        }
      })
      jobs.push(job)
    }

    await Promise.all(jobs)
    console.log("features", pid, jobs.length)

  } else if (name === 'docFeatures') {
    const { documentFeatures } = result as DocumentFeatures
    if (existedFeature) {
      await mongo.elementFeatures.update({
        where: { id: existedFeature.id },
        data: {
          documentFeatures: documentFeatures.screen.concat(documentFeatures.document)
        }
      })
    }
    else {
      console.log("새로 넣을까... 그럼 귀찮긴한데")
    }

  } else {
    if (!test) console.error("데이터가 존재함", test, pid, name)
    dup = true
  }

  ctx.body = {
    status: 'ok',
    pid: pid,
    name: name,
    dup: dup
  }
}

export async function postExtractError(ctx: Context) {
  const { name, pid } = ctx.params
  const result = ctx.request.body

  console.log('에러목록', pid)
  await mongo.extractionError.create({
    data: {
      pid: pid,
      name: name,
      error: 'Error Extraction',
      result
    }
  })
  ctx.body = {
    status: 'ok'
  }
}

export async function getTabNetExtraction(ctx: Context) {
  const { pid } = ctx.params
  const model = parseQuery(ctx.request.query.model)[0]

  const pythonApi = process.env.PYTHON_API
  console.log(pythonApi, pid, model)

  // const positives = await (await fetch(`${pythonApi}/predict/${pid}?${model}`)).json()
  // console.log(positives.length)

  ctx.body = {
    status: 'ok',
    pid: pid,
    positives: 0// positives
  }
}

export async function getErrorPages(ctx: Context) {
  const extracted = await mongo.extractionResult.findMany({
    where: {
      result: {}
    }
  })

  const errors = extracted.filter(x => {
    const { error } = x.result as any
    if (error) return true
    return false
  })

  ctx.body = {
    status: 'ok',
    size: errors.length,
    errors: errors
  }
}

export async function getFeatures(ctx: Context) {
  const { pid } = ctx.params

  const features = await mongo.elementFeatures.findFirst({
    where: { pid: pid }
  })

  ctx.body = {
    status: 'ok',
    data: features
  }
}