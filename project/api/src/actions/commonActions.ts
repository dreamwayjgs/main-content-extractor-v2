import { Context } from "koa"
import { mongo } from "../app"
import { parseQuery } from "../utils";
import { mean, uniq } from "lodash";

export async function getSources(ctx: Context) {
  const answerOnly = parseQuery('answerOnly', 'false')[0]
  const query = answerOnly === 'true' ?
    {
      sample: true,
      savedDate: {
        not: null
      }
    } :
    {
      sample: true,
      savedDate: {
        not: null
      },
      Answer: {
        some: {}
      }
    }
  const sources = await mongo.webpage.findMany({
    where: query,
    select: {
      source: true,
    },
    distinct: ["source"]
  })

  console.log("SOURCE", sources)
  const sourceWithSizes = sources.map(({ source }) => {
    const countQuery = { ...query, source }
    return mongo.webpage.count({
      where: countQuery
    })
  })

  const sizes = await Promise.all(sourceWithSizes)
  const results = sources.map((source, index) => {
    return {
      source: source.source,
      size: sizes[index]
    }
  })

  // sources.push({ source: 'tabnet-samples' })

  ctx.body = {
    status: 'ok',
    sources: results
  }
}

export async function getSource(ctx: Context) {
  const { name } = ctx.params
  const answerOnly = parseQuery(ctx.request.query.answerOnly)[0]

  if (answerOnly === 'true') {
    const answers = await mongo.answer.findMany({
      where: {
        tagType: 'maincontent',
        webpage: {
          source: name
        }
      },
      select: {
        webpage: true
      }
    })

    const pages = answers.map(item => item.webpage)

    ctx.body = {
      status: 'ok',
      size: pages.length,
      pages
    }
  }
  else if (name === 'tabnet-samples') {
    const sampledPid = uniq((await mongo.extractionResult.findMany({
      where: {
        name: {
          contains: 'docsize'
        }
      }
    })).map(r => r.pid))

    const pages = await Promise.all(sampledPid.map(async pid => (await mongo.webpage.findFirst({
      where: {
        id: pid
      },
    }))))

    ctx.body = {
      status: 'ok',
      size: pages.length,
      pages: pages
    }

  } else {
    const pages = await mongo.webpage.findMany({
      where: {
        source: name,
        sample: true,
        savedDate: {
          not: null
        }
      }
    })

    ctx.body = {
      status: 'ok',
      size: pages.length,
      pages: pages
    }
  }
}