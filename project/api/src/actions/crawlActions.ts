import mv from 'mv'
import { Context } from "koa";
import { mongo, postgres } from "../app";
import { sampleSize } from 'lodash';
import { parseQuery } from '../utils';


export async function storePage(ctx: Context) {
  const { id, url, rawHtml, elements, isCompleted } = ctx.request.body as {
    url: string,
    elements: string,
    id: string,
    rawHtml: string,
    isCompleted: boolean
  }

  console.log("[CRAWL] Webpage Saving", id)
  const elems = JSON.parse(elements)

  let filepath = ''
  if (ctx.request.files && ctx.request.files.mhtml) {
    const mhtmlData = Array.isArray(ctx.request.files.mhtml) ? ctx.request.files.mhtml[0] : ctx.request.files.mhtml
    const mhtmlPath = mhtmlData.path
    filepath = `static/mhtml/${id}.mhtml`
    mv(mhtmlPath, filepath, () => {
      console.log("File moved!", filepath)
    })
  }

  const dataStore = await postgres.stored.create({
    data: {
      rawHtml: rawHtml,
      elements: elems
    }
  })

  await mongo.stored.create({
    data: {
      pid: id,
      mhtmlFilePath: filepath,
      dataStoreId: dataStore.id
    }
  })

  await mongo.storedLog.create({
    data: {
      pid: id,
      log: {
        saveOnPageLoad: isCompleted,
      },
      timestamp: new Date()
    }
  })

  await mongo.webpage.update({
    where: {
      id: id
    },
    data: {
      savedDate: new Date(),
    }
  })

  ctx.body = {
    status: 'ok'
  }
}

interface GoogleTrendsKeywords {
  category: string
  keywords: string[]
}

export async function saveGoogleTrendsKeywords(ctx: Context) {
  const { msg, url, data } = ctx.request.body as {
    msg: string
    url: string
    data: GoogleTrendsKeywords[]
  }
  const year = parseInt(url.match(/\d/g)?.join('')!)
  const region = url.split('/').slice(-2)[0]

  const googleTrendsBase = await mongo.googleTrendsBase.create({
    data: {
      year: year,
      url: url,
      region: region
    }
  })

  const reuslt = await mongo.googleTrendsKeywordSet.createMany({
    data: data.map(el => ({ ...el, googleTrendsBaseId: googleTrendsBase.id }))
  })

  console.log("[CRAWL] GoogleTrends Keyword Saved", year, region, reuslt)

  ctx.body = {
    status: 'ok'
  }
}

export async function getGoogleTrendsKeywords(ctx: Context) {
  const data: any[] = []
  const bases = await mongo.googleTrendsBase.findMany()
  for (const base of bases) {
    const keywordSet = await mongo.googleTrendsKeywordSet.findMany({
      where: {
        googleTrendsBaseId: base.id
      }
    })
    data.push({
      ...base,
      keywordSet: keywordSet
    })
  }

  ctx.body = {
    status: 'ok',
    data: data
  }
}

interface GoogleTrendsSource {
  description: any,
  url: string,
  source: string,
  listedDate?: Date
}

export async function postGoogleTrendsSource(ctx: Context) {
  if (Array.isArray(ctx.request.body)) {
    console.log("업뎃", ctx.request.body.length)
    await mongo.webpage.createMany({
      data: ctx.request.body.map((v: any) => ({
        ...v,
        listedDate: new Date()
      }))
    })
  }
  else {
    const { description, url, source, listedDate } = ctx.request.body as GoogleTrendsSource
    await mongo.webpage.create({
      data: {
        url: url,
        source: source,
        listedDate: listedDate ? new Date(listedDate) : new Date(),
        description: description
      }
    })
  }

  ctx.body = {
    status: 'ok'
  }
}

export async function getSourceNamesOfCrawlTarget(ctx: Context) {
  const sources = await mongo.webpage.findMany({
    select: {
      source: true
    },
    distinct: ["source"]
  })

  ctx.body = {
    status: 'ok',
    sources: sources
  }
}

export async function getSources(ctx: Context) {
  const { name } = ctx.params
  const all = parseQuery(ctx.query.all, 'false')[0] === 'true'

  const pages = all ?
    await mongo.webpage.findMany({
      where: {
        source: name,
        sample: true
      }
    }) :
    await mongo.webpage.findMany({
      where: {
        source: name,
        sample: true,
        savedDate: null
      }
    })
  ctx.body = {
    status: 'ok',
    size: pages.length,
    pages: pages
  }
}

export async function makeSamples(ctx: Context) {
  const { name } = ctx.params
  const size = parseQuery(ctx.request.query.size)[0]

  const pages = await mongo.webpage.findMany({
    where: {
      source: name
    }
  })

  console.log(name, size)

  const resultCount = await tagSamples(name, parseInt(size))

  ctx.body = {
    status: 'ok',
    sourceName: name,
    count: resultCount
  }
}

export async function resetSampleStored(ctx: Context) {
  const { name } = ctx.params

  const pages = await mongo.webpage.updateMany({
    where: {
      source: name,
      sample: true
    },
    data: {
      savedDate: null
    }
  })
  ctx.body = {
    status: 'ok',
    size: pages.count
  }
}

async function tagSamples(source: string, size = 300) {
  console.log("SAMPLING", source, size)
  const pages = await mongo.webpage.findMany({
    where: {
      source: source,
      sample: {
        not: true
      }
    }
  })

  console.log("Sample Target", pages.length)

  const updateSamples = sampleSize(pages, size).map(v =>
    mongo.webpage.update({
      where: {
        id: v.id
      },
      data: {
        sample: true
      }
    })
  )

  const updated = await Promise.all(updateSamples)
  return updated.length
}