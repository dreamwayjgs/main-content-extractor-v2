import { Context } from "koa";
import { conformsTo, maxBy } from "lodash";
import { mongo } from "../app";

interface DistAndCenters {
  centerInMainContent: boolean
  name: string
  dist: number
  left: number
  top: number
}

interface CenterResult {
  centerInMainContent: string[]
  closestCenter: string
  mcLeft: number
  mcTop: number
  mcCenter: [number, number]
}

function getCenter(left: number, top: number, width: number, height: number) {
  return {
    left: left + width / 2,
    top: top + height / 2
  }
}

export async function getCenterStats(ctx: Context) {
  const results = await centerStats()

  ctx.body = {
    status: 'ok',
    results: results
  }
}

async function centerStats() {
  const pages = await mongo.centerPosition.findMany()

  const results = []

  for (const centerAndDists of pages) {
    const centerDomRect = await mongo.domRect.findFirst({
      where: {
        id: centerAndDists?.domRectId
      }
    })

    if (centerDomRect === null) {
      console.log("못찾음")
      break
    }

    const mcCenter = getCenter(centerDomRect.left, centerDomRect.top, centerDomRect.width, centerDomRect.height)

    const result: CenterResult = {
      centerInMainContent: [],
      closestCenter: '',
      mcLeft: centerDomRect.left,
      mcTop: centerDomRect.top,
      mcCenter: [mcCenter.left, mcCenter.top],
    }

    // for (const c of centerAndDists?.distAndCenters as any[]) {
    //   if (c.centerInMainContent)
    //     result.centerInMainContent.push(c.name)
    // }

    const mainContentBox: Box = {
      top: centerDomRect.top,
      left: centerDomRect.left,
      right: centerDomRect.right,
      bottom: centerDomRect.bottom
    }


    // Closest Center
    const closest = maxBy(centerAndDists?.distAndCenters, 'dist')
    if (closest) {
      result.closestCenter = (closest as any).name
    }

    results.push(result)
    break
  }
  return results
}

interface Box {
  left: number
  top: number
  right: number
  bottom: number
}

function innerBoxPercentage(outerBox: Box, innerBox: Box) {
  console.log(compareBoxSize(outerBox, innerBox))
}

function compareBoxSize(A: Box, B: Box) {
  if (A.left <= B.left &&
    A.right >= B.right &&
    A.top <= B.top &&
    A.bottom >= B.bottom) {
    return 1 // A is bigger
  } else if (B.left <= A.left &&
    B.right >= A.right &&
    B.top <= A.top &&
    B.bottom >= A.bottom) {
    return -1
  }
  return 0
}