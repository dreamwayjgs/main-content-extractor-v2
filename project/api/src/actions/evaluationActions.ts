import { Context } from "koa";
import { find, flatten, maxBy } from "lodash";
import fetch from "node-fetch";
import { mongo } from "../app";
import { parseQuery } from "../utils";

export async function getAnswer(ctx: Context) {
  const { pid } = ctx.params
  const answers = await mongo.answer.findMany({
    where: {
      pid: pid
    }
  })
  const page = await mongo.webpage.findUnique({
    where: {
      id: pid
    }
  })
  ctx.body = {
    status: 'ok',
    answers: answers,
    page: page
  }
}

interface MozillaParsedDocument<T = string> {
  /** article title */
  title: string;
  /** author metadata */
  byline: string;
  /** content direction */
  dir: string;
  /** HTML of processed article content */
  content: T;
  /** text content of the article (all HTML removed) */
  textContent: string;
  /** length of an article, in characters */
  length: number;
  /** article description, or short excerpt from the content */
  excerpt: string;
  siteName: string;
}

export async function postEvalResult(ctx: Context) {
  const { name, pid } = ctx.params
  const result = ctx.request.body


  if (name === 'mozilla') {
    const { isReadable, article, answerIndex } = result as {
      isReadable: boolean,
      article: MozillaParsedDocument,
      answerIndex: string
    }

    console.log('모질라결과', result)
    // await mongo.evaluationResult.create({
    //   data: {
    //     pid: pid,
    //     name: name,
    //     result: result
    //   }
    // })
  }
  else if (name === 'brave') {
    const { isReadable, content } = result as {
      isReadable: boolean,
      content: string
    }
    // await mongo.evaluationResult.create({
    //   data: {
    //     pid: pid,
    //     name: name,
    //     result: {
    //       isReadable: isReadable,
    //       content: content
    //     }
    //   }
    // })
  }
  ctx.body = {
    status: 'ok'
  }
}

export async function getExtractionResult(ctx: Context) {
  const { pid } = ctx.params
  const name = parseQuery(ctx.request.query.name)[0]
  const startsWith = parseQuery(ctx.request.query.startsWith)[0] === 'true'

  try {
    const page = await mongo.webpage.findFirst({
      where: {
        id: pid
      },
      include: {
        ExtractionResult: true
      }
    })

    if (!page) {
      throw new Error(`Webpage Not Found: ${pid}`)
    }
    const extResults = name === '' ? page.ExtractionResult : page.ExtractionResult.filter(e => {
      return startsWith ? e.name.startsWith(name) : e.name === name
    })

    const extractionResults = extResults.map(({ name, result }) => {
      if (name.startsWith('hyucentroid')) {
        return {
          ...digHyuCentroid(result as any[]),
          name
        }
      }
      else if (name === 'mozilla') {
        const { answerIndex, isReadable } = result as any
        return {
          name: 'mozilla',
          userId: 'mozilla',
          tagType: 'maincontent',
          hyuIndex: answerIndex,
          isReadable: isReadable
        }
      } else if (name === 'brave') {
        console.log("brave")
        console.log(Object.keys(result as any))
        const { isReadable, content } = result as any
        return {
          name: 'brave',
          userId: 'brave',
          tagType: 'maincontent',
          isReadable: isReadable,
          content: content
        }
      } else if (name === 'dom-distiller') {
        const { title, body, raw } = result as any
        return {
          name: 'dom-distiller',
          userId: 'dom-distiller',
          tagType: 'maincontent',
          content: body
        }
      } else if (name === 'boilernet') {
        return {
          name: 'boilernet',
          userId: 'boilernet',
          tagType: 'maincontent',
          content: result
        }
      } else if (name === 'web2text') {
        return {
          name: 'web2text',
          userId: 'web2text',
          tagType: 'maincontent',
          content: result
        }
      } else if (name.startsWith('tabnet')) {
        return {
          name: name,
          userId: name,
          content: result
        }
      }
      else return null
    }).filter(e => e)

    const results = flatten(extractionResults)

    ctx.body = {
      status: 'ok',
      results: results
    }

  }
  catch (err: any) {
    ctx.body = {
      status: 'error',
      error: err.message
    }
  }
}

async function tabnetExtraction(pid: string, model = 'GoogleTrends-2017-basic-60') {
  const pythonApi = process.env.PYTHON_API
  console.log(pythonApi, pid, model)

  const positives = await (await fetch(`${pythonApi}/predict/${pid}?${model}`)).json()
  console.log(positives.length)

  return positives
}

interface ResultData {
  centerName: string
  name: string
  desc: string
  el: string
  seed: string
  data: any
  best?: boolean
}

interface ResultValue {
  spans: number
  spanRatio: number
  spanDensity: number
  texts: number
  textRatio: number
  textDensity: number
  clientRect: any
  seedRect: any
  hyuIndex: string
}

function digHyuCentroid(result: any, alias = 'hyucentroid') {
  const gatherData = (item: ResultData) => {
    try {
      const { centerName, name, desc, el, seed, data, best } = item
      const { spans, texts, hyuIndex } = data as ResultValue
      return [hyuIndex, best ? true : false]
    }
    catch (err) {
      // throw new Error("여기 에러")
      return [-1, false]
    }
  }
  const formatted = {
    desc: `${result.centerName} - ${result.name}`,
    best: gatherData(result)[1],
    userId: alias,
    tagType: 'maincontent',
    hyuIndex: gatherData(result)[0]
  }
  return formatted
}

interface EvaluationMetrics {
  pid: string
  name: string
  metric: string
  answerUser: string
  values: any
  error?: string
}

export async function postMetrics(ctx: Context) {
  const { pid } = ctx.params
  const data = ctx.request.body as EvaluationMetrics[]
  const jobs = data.map(async o => {
    console.log(o.name, o.metric)
    const exist = await mongo.evaluationMetrics.findFirst({
      where: {
        name: o.name,
        metric: o.metric,
        pid: pid
      }
    })
    if (o.name === 'boilernet') {
      console.log(exist)
    }
    if (exist) {
      return Promise.resolve()
    }
    console.log('Update', pid, o.name)
    return mongo.evaluationMetrics.create({
      data: {
        ...o,
        pid: pid
      }
    })
  })

  const existResults = await Promise.all(jobs)
  console.log("Metric updated", pid)

  ctx.body = {
    status: 'ok'
  }
}

export async function getFeatureParams(ctx: Context) {
  const { pid } = ctx.params
  const hyuIndex = parseQuery(ctx.request.query.hyuIndex)[0]
  const queryingMax = parseQuery(ctx.request.query.max)[0]

  // console.log("find", pid, hyuIndex, queryingMax.length, queryingMax !== 'true' ? hyuIndex : 'max')
  const features = await getFeatures(pid, queryingMax === 'true')

  console.log('feature request', pid)

  ctx.body = {
    status: 'ok',
    data: features
  }
}

async function getFeatures(pid: string, lastIndex: boolean, featureType: 'both' | 'basic' = 'both') {
  const features = await mongo.elementFeatures.findMany({
    where: {
      pid: pid
    }
  })

  if (lastIndex) {
    return maxBy(features, o => o.basicFeatures[0])?.basicFeatures[0]
  }

  return features
}

async function getFeaturesByHyuIndex(pid: string, hyuIndex: string, featureType: 'both' | 'basic' = 'both') {
  const features = await mongo.elementFeatures.findMany({
    where: {
      pid: pid
    }
  })

  const el = find(features, o => o.basicFeatures[0].toString() === hyuIndex)
  if (el) {
    const result = [...el.basicFeatures]
    if (featureType === 'both') {
      result.push(el.distanceToCenters[0])
      result.push(el.distanceToCenters[el.distanceToCenters.length - 1])
    }
    return result
  }
  return el
}

export async function postCenterStats(ctx: Context) {
  const { pid } = ctx.params
  const { mainContent, distAndCenters, documentSize } = ctx.request.body
  console.log('post center', pid, mainContent, distAndCenters, documentSize)
  const existing = await mongo.domRect.findFirst({
    where: {
      pid: pid
    }
  })

  if (existing && existing.hyuIndex === mainContent.hyuIndex) {
    console.log("중복 처리?")
  } else {
    const domRect = await mongo.domRect.create({
      data: {
        ...mainContent.domRect,
        pid: pid,
        hyuIndex: mainContent.hyuIndex
      }
    })
    await mongo.centerPosition.create({
      data: {
        domRectId: domRect.id,
        distAndCenters: distAndCenters,
        pid: pid,
        docHeight: documentSize.height,
        docWidth: documentSize.width
      }
    })
  }

  ctx.body = {
    status: 'ok'
  }
}

export async function postNavStats(ctx: Context) {
  const { pid } = ctx.params
  const { allNodes, visibleNodes } = ctx.request.body

  console.log('새로 들어온 데이터', pid)

  const updateValues = async (values: any) => {
    const { domRect, hyuIndex, linkRatioNode, linkRatioText, linkRatioArea, visibleOnly, tagType } = values

    let targetId = ''

    const existing = await mongo.linkNodeValues.findMany({
      where: {
        pid: pid
      }
    })

    for (const ex of existing) {
      // Updates
      const elem = await mongo.domRect.findUnique({
        where: { id: ex.domRectId }
      })

      console.log("이 시점에서 domRect가 존재하냐?", ex.domRectId)
      if (elem) {
        if (elem.hyuIndex === hyuIndex)
          targetId = ex.domRectId
      }
    }

    if (targetId === '') {
      console.log("New data OF ", pid)
      const newDomRect = await mongo.domRect.create({
        data: {
          ...domRect,
          pid: pid,
          hyuIndex: hyuIndex
        }
      })
      await mongo.linkNodeValues.create({
        data: {
          domRectId: newDomRect.id,
          tagType,
          pid,
          linkRatioArea,
          linkRatioNode,
          linkRatioText,
          visibleOnly
        }
      })
    } else {
      // TODO: Check Exsisting linkNodeValues configuration (When update feature is needed)     
      console.log("이 시점에서 domrectId", targetId)

      // if (target !== null)
      await mongo.linkNodeValues.create({
        data: {
          domRectId: targetId,
          tagType,
          pid,
          linkRatioArea,
          linkRatioNode,
          linkRatioText,
          visibleOnly
        }
      })
    }
  }

  await updateValues(allNodes)
  await updateValues(visibleNodes)

  ctx.body = {
    status: 'ok'
  }
}