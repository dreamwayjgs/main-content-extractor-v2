import { Context } from "koa";

export async function greet(ctx: Context) {
  console.log("Hello!")
  const query = ctx.query
  const { id } = ctx.query

  console.log(query)
  console.log(id)

  ctx.body = {
    status: "Hello",
    greet: "Hi!",
  }
}