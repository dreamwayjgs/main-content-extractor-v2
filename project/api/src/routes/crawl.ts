import { getGoogleTrendsKeywords, getSourceNamesOfCrawlTarget, getSources, makeSamples, postGoogleTrendsSource, resetSampleStored, saveGoogleTrendsKeywords, storePage } from "../actions/crawlActions";
import { AppRoute } from "../types";

export const AppRoutes: AppRoute[] = [
  {
    path: '/crawl/source',
    method: 'get',
    action: getSourceNamesOfCrawlTarget
  },
  {
    path: '/crawl/source/:name',
    method: 'get',
    action: getSources
  },
  {
    path: '/crawl',
    method: 'post',
    action: storePage
  },
  {
    path: '/crawl/page',
    method: 'post',
    action: storePage
  },
  {
    path: '/crawl/source',
    method: 'post',
    action: postGoogleTrendsSource
  },
  {
    path: '/crawl/seed/google',
    method: 'get',
    action: getGoogleTrendsKeywords
  },
  {
    path: '/crawl/seed/google',
    method: 'post',
    action: saveGoogleTrendsKeywords
  },
  {
    path: '/crawl/sample/:name',
    method: 'get',
    action: makeSamples
  },
  {
    path: '/crawl/sample/reset/:name',
    method: 'get',
    action: resetSampleStored
  }
]