import { getCenterStats } from "../actions/statisticsActions";
import { AppRoute } from "../types";

export const AppRoutes = [
  {
    path: 'center',
    method: 'get',
    action: getCenterStats
  }
].map(route => ({
  ...route,
  path: `/statistics/${route.path}`
})) as AppRoute[]