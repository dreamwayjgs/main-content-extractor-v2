import { greet } from "./actions/greet";
import { AppRoutes as CrawlRoutes } from './routes/crawl'
import { AppRoutes as CurationRoutes } from './routes/curation'
import { AppRoutes as ExtractionRoutes } from "./routes/extraction";
import { AppRoutes as EvaluationRoutes } from "./routes/evaluation";
import { AppRoutes as StatisticsRoutes } from "./routes/statistics";
import { AppRoute } from "./types";

export const AppRoutes: AppRoute[] = [
  {
    path: '/greet',
    method: 'get',
    action: greet
  },
]

AppRoutes.push(...CrawlRoutes)
AppRoutes.push(...CurationRoutes)
AppRoutes.push(...ExtractionRoutes)
AppRoutes.push(...EvaluationRoutes)
AppRoutes.push(...StatisticsRoutes)