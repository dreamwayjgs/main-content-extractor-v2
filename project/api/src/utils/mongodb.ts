import { connect, Connection } from "mongoose";

let database: Connection;

export const connectMongo = async () => {  // add your own uri below
  const uri = "mongodb://root:1234@mongo:27017/mainContent?authSource=admin"

  if (database) {
    return;
  }

  return connect(uri, {
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
};
